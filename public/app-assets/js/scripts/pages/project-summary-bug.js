/*=========================================================================================
    File Name: project-summary-task.js
    Description: Project Summary Page JS
    ----------------------------------------------------------------------------------------
    Item Name: Stack - Responsive Admin Theme
    Version: 1.0
    Author: GeeksLabs
    Author URL: http://www.themeforest.net/user/geekslabs
==========================================================================================*/

// Basic pie chart
// ------------------------------

$(window).on("load", function(){

    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: '../app-assets/vendors/js/charts/echarts'
        }
    });

    // console.log(JSON.parse($("#data").val()));

    // $("#bug-pie-chart").attr("data").text
    var obj = JSON.parse($("#data").val());

    // Configuration
    // ------------------------------

    require(
        [
            'echarts',
            'echarts/chart/pie',
            'echarts/chart/funnel'
        ],


        // Bug charts setup
        function (ec) {
            // Initialize chart
            // ------------------------------
            var bugChart = ec.init(document.getElementById('bug-pie-chart'));

            // Chart Options
            // ------------------------------
            chartOptions = {

                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    formatter: "{b}: {c} ({d}%)"  //{a} <br/>
                },

                // Add legend
                legend: {
                    show: false,
                    orient: 'horizontal',
                    x: 'left',
                    data: obj.legend
                },

                // Add custom colors
                color: obj.color,

                // Display toolbox
                toolbox: {
                    show: false,
                    orient: 'horizontal',
                    //Enable if you need
                    /*feature: {
                        magicType: {
                            show: true,
                            title: {
                                pie: 'Switch to pies',
                                funnel: 'Switch to funnel',
                            },
                            type: ['pie', 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    y: '20%',
                                    width: '50%',
                                    height: '70%',
                                    funnelAlign: 'left',
                                    max: 1548
                                }
                            }
                        },
                        restore: {
                            show: true,
                            title: 'Restore'
                        },
                        saveAsImage: {
                            show: true,
                            title: 'Same as image',
                            lang: ['Save']
                        }
                    }*/
                },

                // Enable drag recalculate
                calculable: false,

                // Add series
                series: [{
                    name: '',
                    type: 'pie',
                    radius: '70%',
                    center: ['50%', '57.5%'],
                    data: obj.series
                }]
            };

            // Apply options
            // ------------------------------

            bugChart.setOption(chartOptions);


            // Resize chart
            // ------------------------------

            $(function () {

                // Resize chart on menu width change and window resize
                $(window).on('resize', resize);
                $(".menu-toggle").on('click', resize);

                // Resize function
                function resize() {
                    setTimeout(function() {

                        // Resize chart
                        bugChart.resize();
                    }, 200);
                }
            });
        }
    );
});
