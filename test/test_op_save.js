const axios = require('axios');
const assert = require('assert');
const Exercices = require('../models/exercices');
const User = require('../models/user');
// const RegieRubrique = require('../models/regierubriques');
const Centres = require('../models/centres');


describe('Save OP', () => {

  xit('Add OP to Engagement', (done) => {
    const op = {
        numop: "TEST",
        dateop: "12/01/2018",
        objetop: "ksalsla",
        numfacture: "8989",
        datefacture: "12/01/2018",
        numbl: "834878",
        datebl: "12/01/2018",
        montantop: 1200,
        modereglement: "chéque",
        numreglement: "1234",
        comptebancaire: "817281728182718728",
        datereglement: "12/02/2018",
        tmpflagop: true,
        annuler: false
      }

      // console.log(op)

      axios.post('http://localhost:3009/opform/add',{
        exerciceid: "5a88b9b1973dc35f612ae504",
        engageid: "5aa3bff6ff32b943ea77250e",
        op: op
      })
      .then(() => {

        Exercices.findOne({ _id: "5a88b9b1973dc35f612ae504" }, function(err, exercice){
          var engage = exercice.engagements.find(item => item._id == "5aa3bff6ff32b943ea77250e")
          var op = exercice.engagements[exercice.engagements.indexOf(engage)].op.find(item => item.numop === "TEST")
          // console.log(op)
          assert(op.numop === 'TEST')
          op.remove()
            .then(() => {
              exercice.save()
                .then(() => {
                  done()
                })
            })
          // console.log(exercice.engagements[exercice.engagements.indexOf(engage)].op)
        })

        })
      .catch((err) => {
        console.log({message: err})
      })


  })

  xit('Add Regie', (done) => {
    done();
  })

  xit('Add Regie Rubrique', (done) => {
    let regrub = new RegieRubrique({
      center: "5ab2b71aa79da26002ea3fd7",
      rubriques: [
        {
          code: "hdhdhd",
          libelle: "gshshsu",
          creditbudget: 12000,
          montantmax: 5000
        }
      ]
    })
    regrub.save()
      .then(() => {
        RegieRubrique.find()
        .populate('center')
        .then((regie) =>{
          assert(regie[0].center._id == "5ab2b71aa79da26002ea3fd7")
          regrub.remove()
            .then(() => {
              done()
            })
        })
      })
  })

  xit('Test Exercice population region', (done) => {
    Exercices.find({ _id: "5ac0d8ffeb9cbc49078a610c" })
      .populate('regions')
      .then((data) => {
        console.log(data)
        done()
      })
  })

  xit('set rap to 0 and clotured to false for all exercices', (done) => {
    Exercices.update({}, {rap: 0, clotured: false}, {multi: true})
      .then((exercice) => {
          done()
        })
  })

  xit('get users list with populate the region', (done) => {
    User.findOne({ username: 'jalal' })
      .populate('regions')
      .then((users) => {
        console.log(users)
        done()
      })
  })

  xit('test Promise.all', (done) => {
    var one = axios({ url: 'http://localhost:3009/users/api/get/',
      method: 'get',
      // withCredentials: true,
      auth: {
        username: 'kbellioum',
        password: 'test123'
      }
    })
    // var two = axios.get('http://localhost:3009/roles/api/get')
    // var tree = axios.get('http://localhost:3009/regions/api/get')

    axios.all([ one ])
      .then((data) => {
        console.log(data[0].data)
        done()
      })

  })
  xit('get engagement with BC nature', (done) => {
    Exercices.find({ engagements: { $elemMatch: { nature: "BC" }}, clotured: false})
      .then((result) => {
        console.log(result.length)
        done()
      })
  })
  it('nested populate', (done) => {
    Exercices.findOne({_id: "5ad88dbf00a23b0d167f6bf8"})
    .populate('engagements.beneficiaireid')
    .then((data) => {
      console.log(data.engagements)
      done()
    })
  })

})
