var express = require('express');
var router = express.Router();
var Roles = require('../models/roles.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).adm_role == true){
		return next();
	}else{
		res.redirect('/index');
	}
}
/* GET users listing. */

router.get('/:id', isAuthenticated, havePermission, function(req, res, next) {
	Roles.findById(req.params.id,function(err, permissions){
		log('Show', 'Consultation de la liste des permissions du rôle : ' + req.params.id,req.user.username);
		res.render('permissions', {user: req.user, permissions: permissions, message: req.flash('message') });
	});

});

router.get('/api/get/:id',isAuthenticated, havePermission, function(req, res, next) {
	Roles.findById(req.params.id,function(err, roles){
			res.json(roles);
	});

});


router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	Roles.find(function(err, roles){
		res.json(roles);
	});

});

router.post('/api/update/:id',isAuthenticated, havePermission, function(req, res){

		// var nomrole = req.body.nomrole;

		var dashboard = req.body.dashboard;

		var adm_user = req.body.adm_user;
		var adm_role = req.body.adm_role;
		var adm_trace = req.body.adm_trace;

		var param_srcbudget = req.body.param_srcbudget;
		var param_categoriesrubriques = req.body.param_categoriesrubriques;
		var param_rubriques = req.body.param_rubriques;
		var param_benef = req.body.param_benef;
		var param_cptbancaire = req.body.param_cptbancaire;
		var param_entitebenef = req.body.param_entitebenef;
		var param_comptescompta = req.body.param_comptescompta;
		var param_centres = req.body.param_centres;
		var param_regies = req.body.param_regies;
		var param_regions = req.body.param_regions;

		var exer_gestexercice = req.body.exer_gestexercice;
		var exer_crdouvert = req.body.exer_crdouvert;
		var exer_vircrd = req.body.exer_vircrd;
		var exer_projets = req.body.exer_projets;
		var exer_raps = req.body.exer_raps;
		var exer_compta = req.body.exer_compta;

		var regie_budget = req.body.regie_budget;
		var regie_histocredit = req.body.regie_histocredit;
		var regie_jc = req.body.regie_jc;
		var regie_alimcaisse = req.body.regie_alimcaisse;


		var rec_gestrecettes = req.body.rec_gestrecettes;

		var dep_engage = req.body.dep_engage;
		var dep_liquid = req.body.dep_liquid;
		var dep_paie = req.body.dep_paie;

		var etat_crdouvert = req.body.etat_crdouvert;
		var etat_solde = req.body.etat_solde;
		var etat_recettes = req.body.etat_recettes;
		var etat_engage = req.body.etat_engage;
		var etat_annulengage = req.body.etat_annulengage;
		var etat_annulop = req.body.etat_annulop;


		Roles.findById(req.params.id, function (err, roles) {
		roles.update({

		// nomrole: nomrole,

		dashboard: dashboard,

		adm_user: adm_user,
		adm_role: adm_role,
		adm_trace: adm_trace,

		param_srcbudget: param_srcbudget,
		param_categoriesrubriques: param_categoriesrubriques,
		param_rubriques: param_rubriques,
		param_benef: param_benef,
		param_cptbancaire: param_cptbancaire,
		param_entitebenef: param_entitebenef,
		param_comptescompta: param_comptescompta,
		param_centres: param_centres,
		param_regies: param_regies,
		param_regions: param_regions,

		exer_gestexercice: exer_gestexercice,
		exer_crdouvert: exer_crdouvert,
		exer_vircrd: exer_vircrd,
		exer_projets: exer_projets,
		exer_raps: exer_raps,
		exer_compta: exer_compta,

		regie_budget: regie_budget,
		regie_histocredit: regie_histocredit,
		regie_jc: regie_jc,
		regie_alimcaisse: regie_alimcaisse,

		rec_gestrecettes: rec_gestrecettes,

		dep_engage: dep_engage,
		dep_liquid: dep_liquid,
		dep_paie: dep_paie,

		etat_crdouvert: etat_crdouvert,
		etat_solde: etat_solde,
		etat_recettes: etat_recettes,
		etat_engage: etat_engage,
		etat_annulengage: etat_annulengage,
		etat_annulop: etat_annulop

	},function (err, rolesID){
    		if(err){
					log('Update', 'Echec de modification des permissions du rôle : ' + req.params.id,req.user.username);
    			console.log('GET Error: There was a problem retrieving: ' + err);
    		}else{
					log('Update', 'Modification des permissions du rôle : ' + req.params.id,req.user.username);
					res.redirect('/roles');
				}
			})

		 });

	});




module.exports = router;
