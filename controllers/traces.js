var express = require('express');
var router = express.Router();
var Traces = require('../models/traces.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).adm_trace == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

/* GET users listing. */

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Traces.find(function(err, traces){
		log('Show', 'Consultation de la liste des traces.',req.user.username);
		res.render('traces', {user: req.user, traces: traces, message: req.flash('message') });
	}).sort( { dateop: -1 } );

});

router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	Traces.find(function(err, traces){
		res.json(traces);
	}).sort( { dateop: -1 } );

});


router.post('/api/add',isAuthenticated, havePermission, function(req, res){

      var traces = new Traces();
      traces.dateop = req.body.dateop;
      traces.typeop = req.body.typeop;
			traces.description = req.body.description;
			traces.user = req.body.user;



      traces.save(function(err) {
          if (err)
              {res.send(err);}
          else {
            res.json({ message: 'Trace successfully added!' });
          }

      });
});







module.exports = router;
