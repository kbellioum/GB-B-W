var express = require('express');
var router = express.Router();
var Users = require('../models/user.js');
var bCrypt = require('bcrypt-nodejs');
var log = require('../libs/log.js');

var createHash = function(password){
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}


var tool = require("../libs/mylib.js");

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).adm_user == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

/* GET users listing. */

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Users.find(function(err, users){
		res.render('users', {user: req.user, users: users, message: req.flash('message') });
	});

});

// ,isAuthenticated, havePermission

router.get('/api/get', function(req, res, next) {
	Users.find(function(err, users){
		res.json(users);
	});
});

router.delete('/api/delete/:id',isAuthenticated, havePermission, function(req, res){
  var etat ='KO';
  var message = '';
  Users.remove({
    _id: req.params.id
  }, function(err, user) {
    if (err){
      message = 'Echec de suppression de l\'element sélectionné : ' ;
      etat = 'KO';
      log('Delete', 'Echec de suppression de l\'utilisateur : ' + req.params.id,req.user.username);
    }	else {
      log('Delete', 'Suppression de l\'utilisateur  : ' + req.params.id,req.user.username);
      message = 'Suppression avec succes de l\'element sélectionné  : ';
      etat = 'OK';
    }
    res.json({ message: message, etat: etat });
  });
});

router.post('/api/add',isAuthenticated, havePermission, function(req, res){
  var users = new Users();
  users.username = req.body.username;
  users.firstName = req.body.firstName;
  users.lastName = req.body.lastName;
  users.region = req.body.region;
  users.roleid = req.body.roleid;
  users.password = req.body.password;
  users.regisseur = req.body.regisseur;
  users.cin = req.body.cin;
  var etat ='KO';
  var message = '';

  users.save(function(err) {
      if (err){
          message =err;
          etat ='KO';
          log('Insert', 'Echec ajout de l\'utilisateur : ' + req.body.libelle ,req.user.username);
          res.send(err);
        }
      else {
              message ='Opération terminée avec succès';
              etat ='OK';
              Users.find(function(err, users){
              log('Insert', 'Ajout de l\'utilisateur : ' + req.body.libelle ,req.user.username);
              // res.json({ data: entites, message: 'Entite successfully added!' });
              res.json({ data: users, message: message, etat: etat });
              });
          }
  });
});

router.post('/api/update/:id',isAuthenticated, havePermission, function(req, res){
  var username = req.body.username;
  var firstName = req.body.firstName;
  var lastName = req.body.lastName;
  var region = req.body.region;
  var roleid = req.body.roleid;
  var regisseur = req.body.regisseur;
  var cin = req.body.cin;
  var password = (req.body.password.length == 60)? '' : createHash(req.body.password);
  var obj = (req.body.password.length == 60)? {
    username: username,
  	firstName: firstName,
  	lastName: lastName,
  	region: region,
  	roleid: roleid,
  	regisseur: regisseur,
  	cin: cin
  } : {
    username: username,
  	firstName: firstName,
  	lastName: lastName,
  	region: region,
    password: password,
  	roleid: roleid,
    regisseur: regisseur,
    cin: cin
  };
  var etat ='KO';
  var message ='';

  Users.findById(req.params.id, function (err, users) {
    users.update(obj,function (err, userID){

      if(err){
        message =err;
        etat ='KO';
        log('Update', 'Echec modification de l\'utilisateur : ' + req.body.libelle ,req.user.username);
      }else{
        message ='Opération terminée avec succès';
        etat ='OK';
        log('Update', 'Modification de l\'utilisateur : ' + req.body.libelle ,req.user.username);
        res.json({ message: message, etat: etat });
      }
    })

  });
});

module.exports = router;
