var express = require('express');
var router = express.Router();
var Beneficiaires = require('../models/beneficiaires.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

/* GET users listing. */

router.get('/', isAuthenticated, function(req, res, next) {
	Beneficiaires.find(function(err, beneficiaires){
		log('Show', 'Consultation de la liste des bénéficiaires.',req.user.username);
		res.render('beneficiaires', {user: req.user, beneficiaires: beneficiaires, message: req.flash('message') });
	});

});

router.get('/api/get',isAuthenticated,  function(req, res, next) {
	Beneficiaires.find(function(err, beneficiaires){
		res.json(beneficiaires);
	});

});

router.delete('/api/delete/:id',isAuthenticated,  function(req, res){
	var etat ='KO';
	var message = '';
	Beneficiaires.remove({
		_id: req.params.id
	}, function(err, beneficiaire) {
		if (err){
			message = 'Echec de suppression de l\'element sélectionné : ' ;
			etat = 'KO';
			log('Delete', 'Echec de suppression du beneficiaire _id = ' + req.params.id,req.user.username);
			// res.send(err);

		}	else {
			log('Delete', 'Suppression du beneficiaire _id =' + req.params.id,req.user.username);
			message = 'Suppression avec succes de l\'element sélectionné  : ';
			etat = 'OK';
			// res.json({ message: 'OK' });
		}
		res.json({ message: message, etat: etat });
	});

        // Beneficiaires.remove({
        //   _id: req.params.id
        // }, function(err, beneficiaire) {
        //   if (err) {
				// 		log('Delete', 'Echec de suppression du beneficiaire _id = ' + req.params.id,req.user.username);
				// 		res.send(err);
				// 	}
				// 	else {
				// 		log('Delete', 'Suppression du beneficiaire _id =' + req.params.id,req.user.username);
				// 		res.json({ message: 'beneficiaire successfully deleted!' });
				// 	 }
        // });

});

router.post('/api/add',isAuthenticated, function(req, res){
	var beneficiaires = new Beneficiaires();
	beneficiaires.nom = req.body.nom;
	beneficiaires.type = req.body.type;
	beneficiaires.tel = req.body.tel;
	beneficiaires.adresse = req.body.adresse;
	beneficiaires.rib = req.body.rib;
	beneficiaires.autreinfo = req.body.autreinfo;
	var etat ='KO';
	var message = '';

	beneficiaires.save(function(err) {
			if (err){
					message =err;
					etat ='KO';
					log('Insert', 'Echec ajout du beneficiaire ' + req.body.nom,req.user.username);
					res.send(err);
				}
			else {
							message ='Opération terminée avec succès';
							etat ='OK';
							Beneficiaires.find(function(err, beneficiaires){
							log('Insert', 'Ajout du beneficiaire ' + req.body.nom,req.user.username);
							// res.json({ data: entites, message: 'Entite successfully added!' });
							res.json({ data: beneficiaires, message: message, etat: etat });
							});

					}

	});
      // var beneficiaires = new Beneficiaires();
      // beneficiaires.nom = req.body.nom;
      // beneficiaires.type = req.body.type;
			// beneficiaires.tel = req.body.tel;
			// beneficiaires.adresse = req.body.adresse;
			// beneficiaires.rib = req.body.rib;
			// beneficiaires.autreinfo = req.body.autreinfo;
			//
			//
			//
      // beneficiaires.save(function(err) {
      //     if (err) {
			// 			log('Insert', 'Echec ajout du beneficiaire ' + req.body.nom,req.user.username);
			// 			res.send(err);}
      //     else {
			// 						Beneficiaires.find(function(err, beneficiaires){
			// 						log('Insert', 'Ajout du beneficiaire ' + req.body.nom,req.user.username);
			// 						res.json({ data: beneficiaires, message: 'Beneficiaires successfully added!' });
			// 						})
      //          }
			//
      // });
});

router.post('/api/update/:id',isAuthenticated, function(req, res){
	var nom = req.body.nom;
	var type = req.body.type;
	var tel = req.body.tel;
	var adresse = req.body.adresse;
	var rib = req.body.rib;
	var autreinfo = req.body.autreinfo;
	var etat ='KO';
	var message ='';

	Beneficiaires.findById(req.params.id, function (err, beneficiaires) {
		beneficiaires.update({
		nom: nom,
		type: type,
		tel: tel,
		adresse: adresse,
		rib: rib,
		autreinfo: autreinfo

	},function (err, beneficiairesID){
			if(err){
				message =err;
				etat ='KO';
				log('Update', 'Echec de modification du beneficiaire ' + req.body.nom,req.user.username);
				console.log('GET Error: There was a problem retrieving: ' + err);
			}else{
				message ='Opération terminée avec succès';
				etat ='OK';
				log('Update', 'Modification du beneficiaire ' + req.body.nom,req.user.username);
				res.json({ message: message, etat: etat });
			}
		})

	 });
	// 	var nom = req.body.nom;
	// 	var type = req.body.type;
	// 	var tel = req.body.tel;
	// 	var adresse = req.body.adresse;
	// 	var rib = req.body.rib;
	// 	var autreinfo = req.body.autreinfo;
	//
	//
	// 	Beneficiaires.findById(req.params.id, function (err, beneficiaires) {
	// 	beneficiaires.update({
	// 	nom: nom,
	// 	type: type,
	// 	tel: tel,
	// 	adresse: adresse,
	// 	rib: rib,
	// 	autreinfo: autreinfo
	//
	// },function (err, beneficiairesID){
  //   		if(err){
	// 				log('Update', 'Echec de modification du beneficiaire ' + req.body.nom,req.user.username);
  //   			console.log('GET Error: There was a problem retrieving: ' + err);
  //   		}else{
	// 				log('Update', 'Modification du beneficiaire ' + req.body.nom,req.user.username);
	// 				res.json({ message: 'beneficiaire successfully updated!' });
	// 			}
	// 		})
	//
	// 	 });

	});





module.exports = router;
