var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var Beneficiaires = require('../models/beneficiaires.js');
var Rubriques = require('../models/rubriques.js');
var Entites = require('../models/entites.js');
var Projets = require('../models/projets.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).dep_engage == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

router.get('/api/get/:id',isAuthenticated, havePermission, function(req, res, next) {
	Exercice.findById(req.params.id, function(err, exercice){
		res.json(exercice);
	});

});


router.get('/:id', isAuthenticated, havePermission, function(req, res, next){
	var filter = {}
	if(req.user.region.name === "All"){
		filter = {clotured: false}
	}else{
		filter = {region: req.user.region._id, clotured: false}
	}
	var exerciceid = req.params.id.split('|')[0];
	var engageid = req.params.id.split('|')[1];
	Exercices.find(filter,{_id: 1, code: 1, libelle: 1},function(err, exercices){
		Beneficiaires.find({ "type" : "Externe" }, { _id: 1,nom: 1 }, function(err, beneficiairesext){
			Beneficiaires.find({ "type" : "Interne" }, { _id: 1,nom: 1 }, function(err, beneficiairesint){
				Entites.find(function(err, entites){
					Projets.find(function(err, projets){
						Exercices.findById(exerciceid, function(err, exercice){
							var engage = exercice.engagements.filter(item => item._id == engageid)
							var obj = {
								_id: engage[0]._id,
						    montanttotal: engage[0].montanttotal,
						    autreinfos: engage[0].autreinfos,
						    datebc: engage[0].datebc,
						    numbc: engage[0].numbc,
						    nummarche: engage[0].nummarche,
						    nature: engage[0].nature,
						    intitule: engage[0].intitule,
						    objet: engage[0].objet,
						    beneficiaire: engage[0].beneficiaire,
						    beneficiaireid: engage[0].beneficiaireid,
						    dateeng: engage[0].dateeng
							}
							res.render('editengagementsform',{user: req.user, exerciceid: exerciceid, entites: entites, projets: projets, exercices: exercices, beneficiairesext: beneficiairesext, beneficiairesint: beneficiairesint, engagetmp: obj, message: req.flash('message')});
						});

					})

				})

			})

		})

	})

})


router.post('/:id', isAuthenticated, havePermission, function(req, res, next){
console.log("req.body.beneficiaire",req.body.beneficiaire);
	var exerciceid = req.params.id.split('|')[0].trim();
	var engageid = req.params.id.split('|')[1];
  var userid = req.user._id;
	var montanttotal = JSON.parse(req.body.detaileng).reduce(function(a, b) {
    return Number(a) + Number(b.montant);
  }, 0);

	Beneficiaires.findById(req.body.beneficiaire,{ nom: 1},function(err, beneficiaire){
		beneficiairename = beneficiaire.nom;

	var objengage = {
		numeng: "ENG_NO_VAL",
		lastupdate: +new Date(),
		dateeng: req.body.dateeng,
		beneficiaireid: req.body.beneficiaire,
		beneficiaire: beneficiairename,
		objet:req.body.objet,
		intitule: req.body.intitule,
		nature: req.body.nature,
		nummarche: req.body.nummarche,
		numbc: req.body.numbc,
		datebc: req.body.datebc,
		autreinfos: req.body.autreinfos,
		montanttotal: montanttotal,
		rubriques: JSON.parse(req.body.detaileng),
		userid: userid,
		_id: engageid
	}

Exercices.findById(exerciceid,function(err, exercice){

		for (var i=0; i < exercice.engagements.length; i++){
						if (exercice.engagements[i]._id.toString() === engageid) {
							exercice.engagements[i] = objengage;
							break;
						}
			}
			exercice.update({
				 engagements: exercice.engagements
			 },function (err, exercicesID){
				 if(err){
						log('Modification', 'Erreur de modification engagement d\'un montant ' + objengage.montanttotal + 'dhs pour ' + objengage.beneficiaire  ,req.user.username);
					  console.log('GET Error: There was a problem retrieving: ' + err);
				 }else{
					 log('Modification', 'Modification engagement d\'un montant ' + objengage.montanttotal + 'dhs pour ' + objengage.beneficiaire  ,req.user.username);
					//  res.json({ message: 'exercice rubrique budget successfully updated!' });
					 res.redirect('/engagements');
				 }
			 })
   })
 });
})

	function js_yyyy_mm_dd_hh_mm_ss () {
		now = new Date();
		year = "" + now.getFullYear();
		month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
		day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
		hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
		minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
		second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
		return day + "/" + month + "/" + year + "  " + hour + ":" + minute + ":" + second;
	}

module.exports = router;
