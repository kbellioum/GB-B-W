var express = require('express');
var router = express.Router();
var Regiesparam = require('../models/regiesparam.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).param_regies == true){
		return next();
	}else{
		res.redirect('/index');
	}
}
/* GET users listing. = param_entitebenef =*/

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Regiesparam.find(function(err, regiesparam){
		log('Show', 'Consultation de la liste des parametres des regies .',req.user.username);
		res.render('regiesparam', {user: req.user, regiesparam: regiesparam, message: req.flash('message') });
	});

});

router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	Regiesparam.find(function(err, regiesparam){
		console.log('regiesparam', regiesparam);
		res.json(regiesparam);
	});

});


router.get('/api/get/:id',isAuthenticated, havePermission, function(req, res, next) {
	Regiesparam.findById(req.params.id, function(err, regiesparam){
		res.json(regiesparam.rubriques);
	});

});

router.delete('/api/delete/:id',isAuthenticated, havePermission, function(req, res){

        Regiesparam.remove({
          _id: req.params.id
        }, function(err, regieparam) {
          if (err){
						message = 'Echec de suppression de l\'element sélectionné : ' ;
						etat = 'KO';
						log('Delete', 'Echec de suppression de la regie : ' + req.params.id,req.user.username);

						res.send(err);
					}	else {
						message = 'Suppression avec succes de l\'element sélectionné  : ';
						etat = 'OK';
						log('Delete', 'Suppression de la regie : ' + req.params.id,req.user.username);
						res.json({ message: message, etat: etat });
					}



        });

});

router.post('/api/add',isAuthenticated, havePermission, function(req, res){

      var regiesparam = new Regiesparam();
      regiesparam.code = req.body.code;
      regiesparam.libelle = req.body.libelle;
			regiesparam.region = req.body.region;
			regiesparam.regisseur = req.body.regisseur;
      // regiesparam.chef = req.body.chef;
      // regiesparam.autreinfo = req.body.autreinfo;
      // regiesparam.createdate = js_yyyy_mm_dd_hh_mm_ss();
      // regiesparam.usercreate = req.user.username;
      // regiesparam.cloturer = req.body.cloturer;

      regiesparam.save(function(err) {
          if (err){
							message =err;
							etat ='KO';
						  log('Insert', 'Echec ajout de la régie : ' + req.body.libelle ,req.user.username);
							res.send(err);
						}
          else {
								  Regiesparam.find(function(err, regiesparam){
									message ='Opération terminée avec succès';
									etat ='OK';
									log('Insert', 'Ajout de la régie : ' + req.body.libelle ,req.user.username);
									// res.json({ data: entites, message: 'Entite successfully added!' });
									res.json({ data: regiesparam, message: 'OK' });
									});

          		}

      });
});

router.post('/api/update/:id',isAuthenticated, havePermission, function(req, res){

		var code = req.body.code;
		var libelle = req.body.libelle;
		var region = req.body.region;
		var regisseur = req.body.regisseur;


		Regiesparam.findById(req.params.id, function (err, regiesparam) {
		regiesparam.update({
		code: code,
		libelle: libelle,
		region: region,
		regisseur: regisseur

	},function (err, regiesparamID){
    		if(err){
					log('Update', 'Echec modification de la regie : ' + req.body.libelle ,req.user.username);
    			console.log('GET Error: There was a problem retrieving: ' + err);
    		}else{
					log('Update', 'Modification de la regie : ' + req.body.libelle ,req.user.username);
					res.json({ message: 'OK' });
				}
			})

		 });

	});


	function js_yyyy_mm_dd_hh_mm_ss () {
		now = new Date();
		year = "" + now.getFullYear();
		month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
		day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
		hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
		minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
		second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
		return day + "/" + month + "/" + year + "  " + hour + ":" + minute + ":" + second;
	}


module.exports = router;
