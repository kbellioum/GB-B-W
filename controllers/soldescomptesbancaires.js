var express = require('express');
var router = express.Router();
var Soldescomptesbancaires = require('../models/comptesbancaires.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).etat_solde == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

/* GET users listing. = etat_solde = */

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Soldescomptesbancaires.find(function(err, soldescomptesbancaires){
		log('Show', 'Consultation de l\'etat des soldes des comptes bancaires.',req.user.username);
		res.render('soldescomptesbancaires', {user: req.user, soldescomptesbancaires: soldescomptesbancaires, message: req.flash('message') });
	});

});

router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	Soldescomptesbancaires.find(function(err, soldescomptesbancaires){
		res.json(soldescomptesbancaires);
	});

});

module.exports = router;
