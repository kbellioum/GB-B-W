var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");

var message ='';
var etat ='KO';

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).param_entitebenef == true){
		return next();
	}else{
		res.redirect('/index');
	}
}
/* GET users listing. = param_entitebenef =*/
// isAuthenticated, havePermission,
router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	var filter = {}
	if(req.user.region.name === "All"){
		res.json({result: "KO", message: "Vous n'avez pas le status d'un regisseur"})
	}else{
		filter = { clotured: false, region: req.user.region._id }
	}
	Exercices.find(filter)
		.then((data) => {
			var regie = data[0].regies.find((item) => item.regisseur == req.user._id.toString() )
			res.json({result: "OK", caisse: regie.caisse})
		})
});

router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	Entites.find(function(err, entites){
		res.json(entites);
	});
});

router.delete('/api/delete/:id',isAuthenticated, havePermission, function(req, res){
	Entites.findByIdAndRemove(req.params.id, (err, entite) => {
			  if (err){
					message = 'Echec de suppression de l\'element sélectionné : ' ;
					etat = 'KO';
					log('Delete', 'Echec de suppression de l\'entité bénéficiaire : ' + req.params.id,req.user.username);
				}	else {
					log('Delete', 'Suppression de l\'entité bénéficiaire : ' + req.params.id,req.user.username);
					message = 'Suppression avec succes de l\'element sélectionné  : ';
					etat = 'OK';
				}
				res.json({ message: message, etat: etat });
   });
});

router.post('/api/add',isAuthenticated, havePermission, function(req, res){

      var entites = new Entites();
      entites.code = req.body.code;
      entites.libelle = req.body.libelle;
			var etat ='KO';
			var message = '';

      entites.save(function(err) {
          if (err){
							message =err;
							etat ='KO';
						  log('Insert', 'Echec ajout de l\'entité bénéficiaire: ' + req.body.libelle ,req.user.username);
							res.send(err);
						}
          else {
									message ='Opération terminée avec succès';
									etat ='OK';
								  Entites.find(function(err, entites){
									log('Insert', 'Ajout de l\'entité bénéficiaire : ' + req.body.libelle ,req.user.username);
									// res.json({ data: entites, message: 'Entite successfully added!' });
									res.json({ data: entites, message: message, etat: etat });
									});
          		}
      });
});

router.post('/api/update/:id',isAuthenticated, havePermission, function(req, res){

		var code = req.body.code;
		var libelle = req.body.libelle;
		var etat ='KO';
		var message ='';

		Entites.findById(req.params.id, function (err, entites) {
		entites.update({
		code: code,
		libelle: libelle
	},function (err, entitesID){
    		if(err){
					message =err;
					etat ='KO';
					log('Update', 'Echec modification de l\'entité bénéficiaire : ' + req.body.libelle ,req.user.username);
    			console.log('GET Error: There was a problem retrieving: ' + err);
    		}else{
					message ='Opération terminée avec succès';
					etat ='OK';
					log('Update', 'Modification de l\'entité bénéficiaire : ' + req.body.libelle ,req.user.username);
					res.json({ message: message, etat: etat });
				}
			})

		 });

	});





module.exports = router;
