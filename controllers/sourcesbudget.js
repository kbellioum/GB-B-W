var express = require('express');
var router = express.Router();
var Sourcesbudget = require('../models/sourcesbudget.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).param_srcbudget == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

/* GET users listing.  = param_srcbudget = */

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Sourcesbudget.find(function(err, sourcesbudget){
		log('Show', 'Consultation de la liste des sources budgetaires.',req.user.username);
		res.render('sourcesbudget', {user: req.user, sourcesbudget: sourcesbudget, message: req.flash('message') });
	});

});

router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	Sourcesbudget.find(function(err, sourcesbudget){
		res.json(sourcesbudget);
	});

});

router.delete('/api/delete/:id',isAuthenticated, havePermission, function(req, res){
	var etat ='KO';
	var message = '';
	Sourcesbudget.remove({
		_id: req.params.id
	}, function(err, sourcebudget) {
		if (err){
			message = 'Echec de suppression de l\'element sélectionné : ' ;
			etat = 'KO';
			log('Delete', 'Echec de suppression de la source budgetaire : ' + req.params.id,req.user.username);
			// res.send(err);

		}	else {
			log('Delete', 'Suppression de la source budgetaire : ' + req.params.id,req.user.username);
			message = 'Suppression avec succes de l\'element sélectionné  : ';
			etat = 'OK';
			// res.json({ message: 'OK' });
		}
		res.json({ message: message, etat: etat });
	});
        // Sourcesbudget.remove({
        //   _id: req.params.id
        // }, function(err, entite) {
        //   if (err)   {
				// 		 log('Delete', 'Echec de suppression de la source budgetaire : ' + req.params.id,req.user.username);
				// 			res.send(err);
				// 		}
				// 	else {
				// 		log('Delete', 'Suppression de la source budgetaire : ' + req.params.id,req.user.username);
				// 		res.json({ message: 'source budget successfully deleted!' });
				// 	}
				//
        // });

});

router.post('/api/add',isAuthenticated, havePermission, function(req, res){
	var sourcesbudget = new Sourcesbudget();
	sourcesbudget.code = req.body.code;
	sourcesbudget.libelle = req.body.libelle;
	var etat ='KO';
	var message ='';

	  sourcesbudget.save(function(err) {
			if (err){
					message =err;
					etat ='KO';
					log('Insert', 'Echec ajout de la source budgetaire : ' + req.body.libelle ,req.user.username);
					res.send(err);
				}
			else {
							message ='Opération terminée avec succès';
							etat ='OK';
							Sourcesbudget.find(function(err, sourcesbudget){
							log('Insert', 'ajout de la source budgetaire : ' + req.body.libelle,req.user.username);
							// res.json({ data: entites, message: 'Entite successfully added!' });
							res.json({ data: sourcesbudget, message: message, etat: etat });
							});

					}

	});

});

router.post('/api/update/:id',isAuthenticated, havePermission, function(req, res){
		var code = req.body.code;
		var libelle = req.body.libelle;
		var etat ='KO';
		var message ='';

	Sourcesbudget.findById(req.params.id, function (err, sourcesbudget) {
	sourcesbudget.update({
	code: code,
	libelle: libelle
},function (err, sourcesbudgetID){
			if(err){
				message =err;
				etat ='KO';
				log('Update', 'Echec modification de la source budgetaire : ' + req.body.libelle ,req.user.username);
				console.log('GET Error: There was a problem retrieving: ' + err);
			}else{
				message ='Opération terminée avec succès';
				etat ='OK';
				log('Update', 'Modification de la source budgetaire : ' + req.body.libelle  ,req.user.username);
				res.json({ message: message, etat: etat });
			}
		})
	 });
	});





module.exports = router;
