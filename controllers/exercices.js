var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var Rubriques = require('../models/rubriques.js');
var Regiesparam = require('../models/regiesparam.js');
var Counter = require('../models/counter.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).exer_gestexercice == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

/* GET users listing. = exer_gestexercice = */
// function(err, exercices){
// 	log('Show', 'Consultation de la liste des exercices.',req.user.username);
// 	res.render('exercices', {user: req.user, exercices: exercices, message: req.flash('message') });
// }


router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Exercices.find()
	// .populate('Region')
	.then( (exercices) => {
		log('Show', 'Consultation de la liste des exercices.',req.user.username);
		res.render('exercices', {user: req.user, exercices: exercices, message: req.flash('message') });
	});

});
 //get a changer par getcreditouvert
router.get('/api/get/:id',isAuthenticated, havePermission, function(req, res, next) {
	Exercices.findById(req.params.id,function(err, exercice){
			// res.json(exercice);
			res.json(exercice.rubriques);
	});

});


router.get('/api/getvirementcredit/:id',isAuthenticated, havePermission, function(req, res, next) {
	Exercices.findById(req.params.id,function(err, exercice){
			res.json(exercice.virementscredits);
	});

});
router.get('/api/getressources_synthese/:id',isAuthenticated, havePermission, function(req, res, next) {
	var EquipRess_Total =0;
	var FonctRess_Total =0;

	Exercices.findById(req.params.id,function(err, exercice){
		for (var i=0; i < exercice.ressbudget.length; i++){
						if(exercice.ressbudget[i].typebudget.toString() === "EQUIPEMENT"){
							EquipRess_Total += Number(exercice.ressbudget[i].montantprevu);
						} else {
							FonctRess_Total += Number(exercice.ressbudget[i].montantprevu);
						}
					}
					console.log(EquipRess_Total,FonctRess_Total);
			res.json([EquipRess_Total,FonctRess_Total]);
	});


});
// ,isAuthenticated, havePermission
router.get('/api/get2', function(req, res, next) {
	Exercices.aggregate([{ $match: {} },{ $unwind: "$rubriques" },
  	{ $group:{ "_id":{"code": "$code","typerubrique": "$rubriques.typerubrique","year": { $year: "$createddate" }},	"budget": { $sum: "$rubriques.budget" }}},
  	{ $sort: { year: 1 } }],(function (err, exercices){

		if (err)
				{res.send(err);console.log('err');}
		else {
			var filter
			res.json(exercices);
		}
	}))
});

router.get('/api/get3', function(req, res, next){
	var data = []
	var filter = {}
	if(req.user.region.name === "All"){
		filter = {clotured: false, disabled: false }
	}else{
		filter = {region: req.user.region._id, clotured: false, disabled: false }
	}
	Exercices.find(filter, {code: 1, libelle: 1, rubriques: 1, ressbudget: 1, region: 1}).populate('region')
	.then((result) => {
		result.forEach(item => {
			if(item.ressbudget.length === 0 ){
				var resstotal = {montantprevu: 0}
			} else {
					var resstotal = item.ressbudget.reduce(function(a,b){
						return  {montantprevu:  a.montantprevu + b.montantprevu};
					});
			}
			if(item.rubriques.length === 0 ){
				var rubtotal = {budget:0, montantdispo: 0};
			} else {
					var rubtotal = item.rubriques.reduce(function(a,b){
						return {budget: a.budget + b.budget, montantdispo: a.montantdispo + b.montantdispo}
					})
			}

			data.push({_id: item._id, code: item.code, libelle: item.libelle, region: item.region._id, ressourcestotal: resstotal.montantprevu, budgettotal: rubtotal.budget, dispototal: rubtotal.montantdispo})

		})
		res.json(data)
	})
});


router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {

	Exercices.aggregate([{ $project: { code: 1, libelle: 1, region: 1 , date: 1 , disabled: 1 ,user: 1, budgettotal: { $sum: "$rubriques.budget" }, dispototal: { $sum: "$rubriques.montantdispo" }, ressourcestotal: { $sum: "$ressbudget.montantprevu" } } }],(function (err, exercices){

		if (err)
				{res.send(err);console.log('err');}
		else {
			res.json(exercices);
		}

	}))

});

router.delete('/api/delete/:id',isAuthenticated, havePermission, function(req, res){
	var etat ='KO';
	var message = '';
	Exercices.remove({
		_id: req.params.id
	}, function(err, exercice) {
		if (err){
			message = 'Echec de suppression de l\'element sélectionné : ' ;
			etat = 'KO';
			log('Delete', 'Echec de suppression de l\'exercice : ' + req.params.id,req.user.username);

		}	else {
			// Add exercice's counter
			deletecounter_exercice (req.params.id); 

			log('Delete', 'Suppression de l\'exercice : ' + req.params.id,req.user.username);
			message = 'Suppression avec succes de l\'element sélectionné  : ';
			etat = 'OK';
		}
		res.json({ message: message, etat: etat });
	});
});

router.post('/api/add',isAuthenticated, havePermission, function(req, res){
	var datesys = new Date().toISOString();
	Rubriques.find({'active': true} ,function(err, rubriques){
		Regiesparam.find({region: req.body.region})
			.then((Regies) => {
				// console.log(Regies)
	var etat ='KO';
	var message = '';
	var exercices = new Exercices();
	exercices.code = req.body.code;
	exercices.libelle = req.body.libelle;
	exercices.user = req.user.username;
	exercices.date = js_yyyy_mm_dd_hh_mm_ss();
	exercices.region = req.body.region;
	exercices.userlastupdate = req.user.username;
	exercices.datelastupdate = js_yyyy_mm_dd_hh_mm_ss();
	exercices.rubriques = rubriques;
	exercices.regies = Regies;
	exercices.virementscredits = [];
	exercices.engagements = [];

	exercices.save(function(err) {
			if (err){
					message =err;
					etat ='KO';
					log('Insert', 'Echec ajout de l\'exercice : ' + req.body.libelle ,req.user.username);
					res.send(err);
				}
			else {
				// Add exercice's counter
				addcounter_exercice (exercices._id);

							message ='Opération terminée avec succès';
							etat ='OK';
							Exercices.aggregate([{ $match: { clotured: false, disabled: false } }, { $project: { code: 1, libelle: 1, region: 1, date: 1, user: 1, budgettotal: { $sum: "$rubriques.budget" }, dispototal: { $sum: "$rubriques.montantdispo" } } }],(function (err, exercices){
							log('Insert', 'Ajout de l exercice : ' + req.body.code ,req.user.username);
							// res.json({ data: entites, message: 'Entite successfully added!' });
							res.json({ data: exercices, message: message, etat: etat });
						}));
					}
	});
});
});
});

router.post('/api/cloture/:id', function(req, res){
	var exerrap = 0
	Exercices.findOne({_id: req.params.id })
		.populate('region')
		.then( (exercice) => {
			exercice.engagements.forEach((item) => {
				var opstmp = item.op.filter(item => item.tmpflagop === false)
				var tdtmp = Number(item.montanttotal) - Number(opstmp.reduce(function(acc, obj){ return acc, obj.montantop; }, 0))
				item.set({rap: tdtmp})
				exerrap = exerrap + tdtmp
			})
			exercice.set({clotured: true, datecloture: new Date(), rap: exerrap})
			exercice.save()
				.then(() => {
					res.json({ result: "OK", message: "Cloture OK", exercicerap: exerrap })
				})
				.catch((err) => {
					res.json({ result: "KO", message: "Error de cloture" })
				})
		})
});

router.post('/api/disable/:id', function(req, res){
	// var exerrap = 0
	Exercices.findOne({_id: req.params.id })
		// .populate('region')
		.then( (exercice) => {
			// exercice.engagements.forEach((item) => {
			// 	var opstmp = item.op.filter(item => item.tmpflagop === false)
			// 	var tdtmp = Number(item.montanttotal) - Number(opstmp.reduce(function(acc, obj){ return acc, obj.montantop; }, 0))
			// 	item.set({rap: tdtmp})
			// 	exerrap = exerrap + tdtmp
			// })
			exercice.set({ disabled: true })
			exercice.save()
				.then(() => {
					res.json({ result: "OK", message: "Disabled OK" })
				})
				.catch((err) => {
					res.json({ result: "KO", message: "Error de Disabled" })
				})
		})
});

router.get('/api/checkbeforeadd/:id', function(req, res, next) {
	Exercices.find({region: req.params.id, clotured: false})
		.then((result) => {
				if(result.length > 0){
					res.json({result: "KO", message: "Il y a déjá un exercice ouvert pour cette region"})
				}else{
					res.json({result: "OK", message: "Opération réussie"})
				}
			})
});

router.post('/api/update/:id',isAuthenticated, havePermission, function(req, res){

	var etat ='KO';
	var message ='';

	var code = req.body.code;
	var libelle = req.body.libelle;
	var disabled = req.body.disabled;
	var userlastupdate = req.user.username;
	var datelastupdate = js_yyyy_mm_dd_hh_mm_ss();

	Exercices.findById(req.params.id, function (err, exercices) {
				exercices.update({
									code: code,
									libelle: libelle,
									disabled: disabled,
									userlastupdate: userlastupdate,
									datelastupdate: datelastupdate,
									region: req.body.region
},function (err, exercicesID){
			if(err){
				message =err;
				etat ='KO';
				log('Update', 'Echec modification de l\'exercice : ' + req.body.code ,req.user.username);
				console.log('GET Error: There was a problem retrieving: ' + err);
			}else{
				message ='Opération terminée avec succès';
				etat ='OK';
				log('Update', 'Modification de l\'exercice : ' + req.body.code ,req.user.username);
				res.json({ message: message, etat: etat });
			}
		})

	 });
});

function addcounter_exercice (exerciceid) {
	var counter = new Counter();
	counter.exerciceid = exerciceid;
	// counter.name = name;
	counter.save()
    .then(result => {
        console.log("add counter ok");  // this will be the new created ObjectId
    })
}

function deletecounter_exercice (exerciceid) {
	Counter.remove({ exerciceid: exerciceid }, function() {
    // removed.
});
}

	function js_yyyy_mm_dd_hh_mm_ss () {
	  now = new Date();
	  year = "" + now.getFullYear();
	  month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
	  day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
	  hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
	  minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
	  second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
	  return day + "/" + month + "/" + year + "  " + hour + ":" + minute + ":" + second;
	}

module.exports = router;
