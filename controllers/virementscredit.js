var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).exer_crdouvert == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

/* GET users listing. = exer_crdouvert =*/

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	var filter = {}
	if(req.user.region.name === "All"){
		filter = {clotured: false, "vcvalide" : false}
	}else{
		filter = {region: req.user.region._id, clotured: false, "vcvalide" : false}
	}
	Exercices.find(filter, { _id: 1,libelle: 1 },function(err, exercices){
		log('Show', 'Consultation de la liste des virements de credit.',req.user.username);
		res.render('virementscredit', {user: req.user, exercices: exercices, message: req.flash('message') });
	}).sort( { createddate: -1 } );

});


router.post('/api/update/:id',isAuthenticated, havePermission, function(req, res){
console.log('/api/update/:id');
	  var exeid = req.query.exeid;
	  var virementcredittmp = req.body.virementcredittmp;
		var message ='';
		var etat ='';

	Exercices.findById(exeid,function(err, exercices){

      for (var i=0; i < exercices.rubriques.length; i++){

              if(exercices.rubriques[i]._id.toString() === req.params.id){
								if(exercices.rubriques[i].montantdispo + Number(virementcredittmp) >= 0){
										var tt = exercices.rubriques[i];
		                tt.virementcredittmp = Number(virementcredittmp);
										// tt.montantdispo = tt.virementcredit + Number(virementcredittmp) - tt.totalengagements;
		                exercices.rubriques[i] = tt;
										message = 'Modification du virement de credit de la rubrique ' + exercices.rubriques[i].code + ' de l exercice [' + exercices.code + ']';
										etat = 'OK';
			          }else{
									  message = 'Attention, Vous aves dépassé le budget disponible.';
			              etat = 'KO';
			          }
								break;
              }
        }
       exercices.update({
          rubriques: exercices.rubriques
        },function (err, exercicesID){
			    		if(err){
								etat = 'KO';
								log('Update', message ,req.user.username);
			    			console.log('GET Error: There was a problem retrieving: ' + err);
			    		}else{
								log('Update', message ,req.user.username);
								res.json({ message: message, etat: etat });
							}
						})
      });

	});
router.post('/api/valider/:id',isAuthenticated, havePermission, function(req, res){
	  var exeid = req.params.id;
		var message ='';
		var etat ='OK';

	Exercices.findById(exeid,function(err, exercices){
      for (var i=0; i < exercices.rubriques.length; i++){
						exercices.rubriques[i].virementcredit = exercices.rubriques[i].virementcredittmp;
						exercices.rubriques[i].montantdispo += Number(exercices.rubriques[i].virementcredittmp);
						if (Number(exercices.rubriques[i].montantdispo)< 0) {
							etat = 'KO';
							message = 'Attention opération annulée a cause du dépassement du budget disponible de la rubrique ' + exercices.rubriques[i].code;
							break;
						}
        }
				if (etat === 'OK') {
					exercices.update({
						 rubriques: exercices.rubriques,
						 vcvalide : true
					 },function (err, exercicesID){
						 if(err){
							 log('Update', message ,req.user.username);
							 console.log('GET Error: There was a problem retrieving: ' + err);
							 res.json({ message: message + err });
						 }else{
							 log('Update', message ,req.user.username);
							 res.json({ message: 'Virement de crédit appliqué avec succès.', etat: etat });
						 }
					 })
				} else {
					log('Update', message ,req.user.username);
					res.json({ message: message, etat: etat });
				}

      });

	});





module.exports = router;
