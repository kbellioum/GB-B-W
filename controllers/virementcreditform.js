var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).exer_vircrd == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

/* GET users listing. = exer_vircrd = */

router.get('/:id', isAuthenticated, havePermission, function(req, res, next) {
	var rubriquesequipement = [];
	var rubriquesfonctionnement = [];
	var exeid = req.params.id;

  Exercices.findById(exeid,function(err, exercices){

		for(i=0; i< exercices.rubriques.length; i++){
		     if ( exercices.rubriques[i].type == "EQUIPEMENT" ) {

		         rubriquesequipement.push( exercices.rubriques[i] );

		     }else {

					 rubriquesfonctionnement.push( exercices.rubriques[i] );
				 }
 		}
		res.render('virementcreditform', {user: req.user,exeid: exeid, rubriquesequipement: rubriquesequipement,rubriquesfonctionnement: rubriquesfonctionnement, message: req.flash('message') });

	});

});


router.post('/api/add/:id',isAuthenticated, havePermission, function(req, res){

	  var exeid = req.params.id;
		var idrubriquesource = req.body.rsource;
		var idrubriquedestination = req.body.rdestination;
		var montant = Number(req.body.montant);
		var motif = req.body.motif;

		var coderubriquesource ='' ;
		var coderubriquedestination = '';
		// var log = '';
		var tt;
		var derogation;

		//

		Exercices.findById(exeid,function(err, exercices){

			for (var i=0; i < exercices.rubriques.length; i++){

							if (exercices.rubriques[i]._id.toString() === idrubriquesource && exercices.rubriques[i].montantdispo >= Number(montant)) {
								coderubriquesource = exercices.rubriques[i].code;
								tt = exercices.rubriques[i];
								// tt.budget -= Number(montant);
								tt.virementcredit -= Number(montant);
								tt.montantdispo -= Number(montant);
								exercices.rubriques[i] = tt;
								// log = log + 'Débit rubrique source ok. '

							} else if (exercices.rubriques[i]._id.toString() === idrubriquedestination ){
								coderubriquedestination = exercices.rubriques[i].code;
								tt = exercices.rubriques[i];
								// tt.budget += Number(montant);
								tt.virementcredit += Number(montant);
								tt.montantdispo += Number(montant);
								exercices.rubriques[i] = tt;
								// log = log + 'Crédit rubrique source ok. '
							}
				}
// ---------------------------------------------------------------

				if(coderubriquesource!==''){

								derogation = {
										coderubriquesource: coderubriquesource,
										coderubriquedestination: coderubriquedestination,
										montant: montant,
										user: req.user.username,
										date: js_yyyy_mm_dd_hh_mm_ss(),
										motif: motif
									};
									exercices.virementscredits.push(derogation);
// -----------------------------------------------------------------
								exercices.update({
									 rubriques: exercices.rubriques,
									 virementscredits: exercices.virementscredits
								 },function (err, exercicesID){
									 if(err){
										  log('Insert', 'Transfert du virement de credit ' + montant + 'dhs de la rubrique ' + coderubriquesource + ' vers la rubrique ' +  coderubriquedestination ,req.user.username);
										 console.log('GET Error: There was a problem retrieving: ' + err);
									 }else{
										  log('Insert', 'Transfert du virement de credit ' + montant + 'dhs de la rubrique ' + coderubriquesource + ' vers la rubrique ' +  coderubriquedestination ,req.user.username);
										//  res.json({ message: 'exercice rubrique budget successfully updated!' });
										res.redirect('/virementscredits');
									 }
								 })

// -----------------------------------------------------------------
					}else {
						res.redirect('/virementscredits');
					}
				});
});



	function js_yyyy_mm_dd_hh_mm_ss () {
		now = new Date();
		year = "" + now.getFullYear();
		month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
		day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
		hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
		minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
		second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
		return day + "/" + month + "/" + year + "  " + hour + ":" + minute + ":" + second;
	}

module.exports = router;
