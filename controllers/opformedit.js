var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var ComptesBancaires = require('../models/comptesbancaires.js');
var Comptescomptas = require('../models/comptescomptas.js');
var Beneficiaires = require('../models/beneficiaires.js');

var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).dep_engage == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

router.get("/:id", function(req, res, next){
	var exerciceid = req.params.id.split('|')[0];
	var engageid = req.params.id.split('|')[1];
	var opid = req.params.id.split('|')[2];
	Exercices.findById(exerciceid, function(err, exercice){
		var engage = exercice.engagements.find(item => item._id == engageid)
		var op = exercice.engagements[exercice.engagements.indexOf(engage)].op.find(item => item._id == opid)
		// res.json({result: "OK", op: op})
		ComptesBancaires.find({},{_id: 1, code: 1, banque:1, comptebancaire: 1},function(err, comptesbancaires){
				 Comptescomptas.find({ },function(err, comptescomptas){
					 Beneficiaires.find({ "type" : "Externe" }, { _id: 1,nom: 1 },function(err, beneficiairesext){
						 Beneficiaires.find({ "type" : "Interne" }, { _id: 1,nom: 1 },function(err, beneficiairesint){
				 		 		res.render('opformedit', {user: req.user, op: op, engagement: engage, exercices: exercice, comptesbancaires: comptesbancaires,beneficiairesext: beneficiairesext,beneficiairesint: beneficiairesint,  comptescomptas: comptescomptas, message: req.flash('message') });
			 			 });
		 			});
 				});
 		});
	})

});

router.post('/edit', function(req, res, next){

	var op = req.body.op;
	var exerciceid = req.body.exerciceid.trim();
	var engageid = req.body.engageid.trim();
	if(op === undefined || op === null){
		res.json({result: 'KO', data: req.body})
	}else{
		Exercices.findById(exerciceid, function(err, exercice){
			var engage = exercice.engagements.find(item => item._id == engageid)
			exercice.engagements[exercice.engagements.indexOf(engage)].op.push(op)

			// res.json({result: "OK", data: engage})
			exercice.save()
			.then(function(err){
				res.json({result: "OK", message: "Opération réussie", engagement: engage, tmpflag: exercice.engagements[exercice.engagements.indexOf(engage)].tmpflag})
			})
			.catch(function(err){
				res.json({result: "KO", message: "Il y a eu un problème", err: err})
				console.log("err",err);
			})

		})
	}


})

function js_yyyy_mm_dd_hh_mm_ss () {
	now = new Date();
	year = "" + now.getFullYear();
	month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
	day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
	hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
	minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
	second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
	return day + "/" + month + "/" + year + "  " + hour + ":" + minute + ":" + second;
}

module.exports = router;
