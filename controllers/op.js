var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');

var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).dep_engage == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
		Exercices.find({ "disabled" : false },{ _id: 1,libelle: 1 }, function(err, exercices){
			log('Show', 'Consultation de la liste des ordres de paiement.',req.user.username);
			res.render('ops', {user: req.user, exercices: exercices, message: req.flash('message') });
		});
});


router.get('/api/get/:id',isAuthenticated, havePermission, function(req, res, next) {
var tabop = [];
	Exercices.findById(req.params.id,function(err,exercice){
				for (var i=0; i < exercice.engagements.length; i++){
					for (var j=0; j < exercice.engagements[i].op.length; j++){
							tabop.push(
								{				"op_id": exercice.engagements[i].op[j]._id,
								 				"eng_id": exercice.engagements[i]._id,
								 				"numeng": exercice.engagements[i].numeng,
												"beneficiaire": exercice.engagements[i].beneficiaire,
												"nature": exercice.engagements[i].nature,
												"numop": exercice.engagements[i].op[j].numop,
												"dateop": exercice.engagements[i].op[j].dateop,
												"objetop": exercice.engagements[i].op[j].objetop,
												"montantop": exercice.engagements[i].op[j].montantop,
												"tmpflagop": exercice.engagements[i].op[j].tmpflagop
								}
						  )
				}}
				// console.log(tabop);
		res.json(tabop);
	});
});

router.delete('/api/delete/:id', isAuthenticated, havePermission, function(req, res){
	var etat ='KO';
	var message ='';
	Exercices.findOneAndUpdate({'engagements._id':req.params.id},
			{
			  $pull: { engagements: {_id:req.params.id}}
			},{new:true},
			function(err, engagement) {
			  if(err){
					message = 'Echec de suppression de l\'element sélectionné : ' ;
					etat = 'KO';
					log('Delete', 'Echec de suppression de l\'engagement : ' + req.params.id,req.user.username);
				}else{
					message = 'Suppression avec succes de l\'element sélectionné';
					etat = 'OK';
					log('Delete', 'Suppression de l\'engagement : ' + req.params.id,req.user.username);
				}
				console.log('etat final : ', etat);
				res.json({ message: message, etat: etat });
			})

});


module.exports = router;
