var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).etat_crdouvert == true){
		return next();
	}else{
		res.redirect('/index');
	}
}


/* GET users listing. = etat_crdouvert = */

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Exercices.find({ }, { _id: 1,libelle: 1 },function(err, exercices){
		log('Show', 'Consultation de l\'etat des credits ouverts.',req.user.username);
		res.render('etatcreditsouverts', {user: req.user, exercices: exercices, message: req.flash('message') });
	});

});

// 
// router.post('/api/update/:id',isAuthenticated, havePermission, function(req, res){
//
// 	  var exeid = req.query.exeid;
// 	  var budget = req.body.budget;
// 		var message ='';
// 	Exercices.findById(exeid,function(err, exercices){
//
//       for (var i=0; i < exercices.rubriques.length; i++){
//
//               if(exercices.rubriques[i]._id.toString() === req.params.id){
// 								if(exercices.rubriques[i].budget  === exercices.rubriques[i].montantdispo &&  exercices.rubriques[i].virementcredit === 0){
// 										var tt = exercices.rubriques[i];
// 		                tt.budget = Number(budget);
// 										tt.montantdispo = Number(budget);
// 		                exercices.rubriques[i] = tt;
// 										message = 'Mise a jour du credit ouvert avec succes';
// 			          }else{
// 			              message = 'Impossible de modifier le crédit ouvert. La rubrique est liés á des engagements ou á des virements de crédit!';
// 			          }
// 								break;
//
//               }
//         }
//        exercices.update({
//           rubriques: exercices.rubriques
//         },function (err, exercicesID){
//
//
//           if(err){
//             console.log('GET Error: There was a problem retrieving: ' + err);
// 						res.json({ message: message + err });
//           }else{
//
//             res.json({ message: message });
//           }
//
//
//         })
//
//       });
//
// 	});





module.exports = router;
