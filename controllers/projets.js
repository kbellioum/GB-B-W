var express = require('express');
var router = express.Router();
var Projets = require('../models/projets.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).exer_projets == true){
		return next();
	}else{
		res.redirect('/index');
	}
}
/* GET users listing. = param_entitebenef =*/

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Projets.find(function(err, projets){
		log('Show', 'Consultation de la liste des projets.',req.user.username);
		res.render('projets', {user: req.user, projets: projets, message: req.flash('message') });
	});

});

router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	Projets.find(function(err, projets){
		res.json(projets);
	});

});


router.get('/api/get/:id',isAuthenticated, havePermission, function(req, res, next) {
	Projets.findById(req.params.id, function(err, projets){
		res.json(projets.emploi);
	});

});

router.delete('/api/delete/:id',isAuthenticated, havePermission, function(req, res){

        Projets.remove({
          _id: req.params.id
        }, function(err, projet) {
          if (err){
						log('Delete', 'Echec de suppression du projet : ' + req.params.id,req.user.username);
						res.send(err);
					}	else {
						log('Delete', 'Suppression du projet : ' + req.params.id,req.user.username);
						res.json({ message: 'OK' });
					}



        });

});

router.post('/api/add',isAuthenticated, havePermission, function(req, res){

      var projets = new Projets();
      projets.code = req.body.code;
      projets.libelle = req.body.libelle;
      projets.chef = req.body.chef;
      projets.autreinfo = req.body.autreinfo;
      projets.createdate = js_yyyy_mm_dd_hh_mm_ss();
      projets.usercreate = req.user.username;
      projets.cloturer = req.body.cloturer;



      projets.save(function(err) {
          if (err){
						message =err;
						result ='KO';
						  log('Insert', 'Echec ajout du projet : ' + req.body.libelle ,req.user.username);
							res.send(err);
						}
          else {
								  Projets.find(function(err, projets){
									log('Insert', 'Ajout du projet : ' + req.body.libelle ,req.user.username);
									// res.json({ data: entites, message: 'Entite successfully added!' });
									res.json({ data: projets, result: "OK", message: "Opération réussie" });
									});

          		}

      });
});

router.post('/api/update/:id',isAuthenticated, havePermission, function(req, res){

		var code = req.body.code;
		var libelle = req.body.libelle;
		var chef = req.body.chef;
		var autreinfo = req.body.autreinfo;
		var createdate = js_yyyy_mm_dd_hh_mm_ss();
		var usercreate = req.user.username;
		var cloturer = req.body.cloturer;

		Projets.findById(req.params.id, function (err, projets) {
		projets.update({
		code: code,
		libelle: libelle,
		chef: chef,
		autreinfo: autreinfo,
		createdate: createdate,
		usercreate: usercreate,
		cloturer: cloturer

	},function (err, projetsID){
    		if(err){
					log('Update', 'Echec modification du projet : ' + req.body.libelle ,req.user.username);
    			console.log('GET Error: There was a problem retrieving: ' + err);
    		}else{
					log('Update', 'Modification du projet : ' + req.body.libelle ,req.user.username);
					res.json({ message: 'OK' });
				}
			})

		 });

	});


	function js_yyyy_mm_dd_hh_mm_ss () {
		now = new Date();
		year = "" + now.getFullYear();
		month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
		day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
		hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
		minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
		second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
		return day + "/" + month + "/" + year + "  " + hour + ":" + minute + ":" + second;
	}


module.exports = router;
