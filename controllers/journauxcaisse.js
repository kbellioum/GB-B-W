var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var Rubriques = require('../models/rubriques.js');
var Entites = require('../models/entites.js');
var EngageTmp = require('../models/engagetmp.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");
var Counter = require('../models/counter.js');



var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).regie_jc == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

router.get('/get_jc_emploi/:id', function(req, res, nex) {
	var exerciceid = req.params.id.toString().split('|')[0]
	var regieid = req.params.id.toString().split('|')[1]
	var coderub = req.params.id.toString().split('|')[2]

	Exercices.find({ _id: exerciceid })
		.then((result) => {
			var regie = result[0].regies.find(item => item._id == req.params.id.toString().split('|')[1])
			var emploi = regie.emploi.find(item => item.code.trim() == coderub.trim())
			res.json({message: "OK", plafond: emploi.plafond})
		})

})

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	var filter = {}
	if(req.user.region.name === "All"){
		filter = {clotured: false}
	}else{
		filter = {region: req.user.region._id, clotured: false}
	}
		Exercices.find(filter,{ _id: 1,libelle: 1 }, function(err, exercices){
			Entites.find(function(err, entites){
					// Rubriques.find({ code: 1, libelle: 1},function(err, rubriques){
							log('Show', 'Consultation de la liste des engagements.',req.user.username);
							res.render('journauxcaisse', {user: req.user, exercices: exercices,entites:entites,  message: req.flash('message') });
						// });
					});
		});
});
// ,isAuthenticated, havePermission
router.get('/api/get/:id', function(req, res, next) {
	Exercices.findById(req.params.id,function(err,exercice){
		var obj = {}
		var tb = []
		if(exercice.regies.length > 0){
			exercice.regies.forEach(item => {
				obj = {
					_id: item._id,
					libelle: item.libelle
				}
				tb.push(obj)
			})
		}


		res.json(tb);
	});
});

router.get('/api/getjc/:id', function(req, res, next) {
	var exerciceid = req.params.id.split('|')[0]
	var regieid = req.params.id.split('|')[1]
	var jc_tmp = [];

	if(regieid !== "vide" && exerciceid !== "vide"){
		var total_credit = 0;
		var total_debit = 0;
		Exercices.findById({_id: exerciceid})
			.then((data) => {
				var regie = data.regies.find(item => item._id == regieid)
				// calcul total creditouvert
				for (var j=0; j < regie.caisse.length; j++){
					total_credit += regie.caisse[j].montant;
				}
 			// fin calcul total creditouvert
				for (var i=0; i < regie.journauxdecaisse.length; i++){
					// calcul somme des depenses detail jc
									if(regie.journauxdecaisse[i].detail.length === 0 ){
										var totaldepense = {montant:0};
									} else {
										 var totaldepense = regie.journauxdecaisse[i].detail.reduce(function(a,b){
												return {montant: a.montant + b.montant}
											})
									}
									total_debit += totaldepense.montant;
					// personnaliser les donnees jc
									jc_tmp.push({
										"_id": regie.journauxdecaisse[i]._id,
										"num": regie.journauxdecaisse[i].num,
										"datedebut": regie.journauxdecaisse[i].datedebut,
										"datepostval": regie.journauxdecaisse[i].datepostval,
										"dateval": regie.journauxdecaisse[i].dateval,
										"detail": regie.journauxdecaisse[i].detail,
										"totaldepense":totaldepense.montant.toFixed(2),

									})
				}
				//
	// console.log("regie",regie.libelle,total_credit ,parseFloat(total_debit).toFixed(2) );
				res.json({result: "OK", regid: regieid, jc: jc_tmp, emploi: regie.emploi,total_credit: total_credit, total_debit:total_debit });

				// res.json(data.regies);
			})
	}


});

function gettotal_encaiss_regie (regie) {
				var total_credit = 0;
				var total_debit = 0;
			// calcul total encaissements
				for (var j=0; j < regie.caisse.length; j++){
					total_credit += regie.caisse[j].montant;
				}
				// calcul total depenses
				for (var i=0; i < regie.journauxdecaisse.length; i++){
					// calcul somme des depenses detail jc
									if(regie.journauxdecaisse[i].detail.length === 0 ){
										var totaldepense = {montant:0};
									} else {
										 var totaldepense = regie.journauxdecaisse[i].detail.reduce(function(a,b){
												return {montant: a.montant + b.montant}
											})
									}
						total_debit += totaldepense.montant;
					}
					return {"total_credit":total_credit, "total_debit": total_debit}
}

router.get('/api/getcreditouvert/:id', function(req, res, next) {
	var exerciceid = req.params.id.split('|')[0]
	var regieid = req.params.id.split('|')[1]

	if(regieid !== "vide" && exerciceid !== "vide"){
		Exercices.findById({_id: exerciceid})
			.then((data) => {
				var regie = data.regies.find(item => item._id == regieid)
				res.json({result: "OK", regid: regieid, emploi: regie.emploi});

				// res.json(data.regies);
			})
	}


});

router.post('/api/jcaisse/add', function(req, res, next) {
	var d = new Date()
	var exerciceid = req.body.exerciceid;
	var regieid = req.body.regieid;
	var jcaisse = req.body.jcaisse;

	// Counter.findOneAndUpdate({exerciceid: exerciceid, name: 'counterjc'}, {$inc: { seq: 1} }, function(error, counter){
	Counter.findOneAndUpdate({exerciceid: exerciceid}, {$inc: { seq_counterjc: 1} }, function(error, counter){
		var jcnum = "JC".concat("00000".substring(0,"00000".length - counter.seq_counterjc.toString().length), counter.seq_counterjc).concat("/").concat(d.getMonth()+1).concat("/").concat(d.getFullYear())
		jcaisse.num = jcnum
		Exercices.findById({_id: exerciceid})
			.then((data) => {
				var regie = data.regies.find(item => item._id == regieid)
				regie.journauxdecaisse.push(jcaisse)
				data.save()
				.then(function(err){
					res.json({result: "OK", message: "Opération réussie",journauxdecaisse: regie.journauxdecaisse })
				})
				.catch(function(err){console.log("error",err);
					res.json({result: "KO", message: "Il y a eu un problème" })
				})
			})
	})
});
//  delete jc
router.post('/api/jcaisse/remove', function(req, res, next) {
	var exerciceid = req.body.exerciceid;
	var regieid = req.body.regieid;
	var jcaisseelmid = req.body.jcaisseelmid;

	Exercices.findById({_id: exerciceid})
		.then((data) => {
			var regie = data.regies.find(item => item._id == regieid)
			var element = regie.journauxdecaisse.find(item => item._id == jcaisseelmid)
			if(element === undefined){
				res.json({result: "KO", message: "l'element n'existe pas"});
			}else{
				element.remove()
					.then(() => {
						data.save()
							.then(() => {
								res.json({result: "OK", message: "Opération réussie", elem: element});
							})
					})
			}

			// res.json(data.regies);
		})

});
// delete ligne detail jc
router.post('/api/jcaisse/detail_jc/remove', function(req, res, next) {
	var exerciceid = req.body.exerciceid;
	var regieid = req.body.regieid;
	var jcaisseelmid = req.body.jcaisseelmid;
	var detail_jc_id = req.body.detail_jc_id;

	Exercices.findById({_id: exerciceid})
		.then((data) => {
			var regie = data.regies.find(item => item._id == regieid)
			var jc = regie.journauxdecaisse.find(item => item._id == jcaisseelmid)
			var element_detail_jc = jc.detail.find(item => item._id == detail_jc_id)
			if(element_detail_jc === undefined){
				res.json({result: "KO", message: "l'element n'existe pas"});
			}else{
				element_detail_jc.remove()
					.then(() => {
						data.save()
							.then(() => {
								res.json({result: "OK", message: "Opération réussie", obj: element_detail_jc});
							})
					})
			}

			// res.json(data.regies);
		})

});



router.post('/api/jcaisse/update_detail_jc', function(req, res, next){
	var exerciceid = req.body.exerciceid;
	var regieid = req.body.regieid;
	var jcaisseelmid = req.body.jcaisseelmid;
	var obj = req.body.obj_detail_jc;
	Exercices.findById({_id: exerciceid})
		.then((data) => {
				var regie = data.regies.find(item => item._id == regieid)

				var jc = regie.journauxdecaisse.find(item => item._id == jcaisseelmid)

				jc.detail.push(obj)
				data.save()
				.then(function(err){
					res.json({obj: jc.detail, result: "OK", message: "Opération réussie" })
				})
				.catch(function(err){
					res.json({ result: "KO", message: "Il y a eu un problème", err: err})
				})



	})

})



router.delete('/api/delete/:id', isAuthenticated, havePermission, function(req, res){
	var etat ='KO';
	var message ='';
	Exercices.findOneAndUpdate({'engagements._id':req.params.id},
			{
			  $pull: { engagements: {_id:req.params.id}}
			},{new:true},
			function(err, engagement) {
			  if(err){
					message = 'Echec de suppression de l\'element sélectionné : ' ;
					etat = 'KO';
					log('Delete', 'Echec de suppression de l\'engagement : ' + req.params.id,req.user.username);
				}else{
					message = 'Suppression avec succes de l\'element sélectionné';
					etat = 'OK';
					log('Delete', 'Suppression de l\'engagement : ' + req.params.id,req.user.username);
				}
				res.json({ message: message, etat: etat });
			})

});


module.exports = router;
