var express = require('express');
var router = express.Router();
var Regions = require('../models/region.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");

var message ='';
var etat ='KO';


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).param_regions == true){
		return next();
	}else{
		res.redirect('/index');
	}
}
/* GET users listing. = param_entitebenef =*/

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Regions.find(function(err, regions){
		log('Show', 'Consultation de la liste des centres.',req.user.username);
		res.render('regions', {user: req.user, regions: regions, message: req.flash('message') });
	});
});


// isAuthenticated, havePermission,
router.get('/api/get/:id', function(req, res, next) {
	Regions.find((req.params.id === "notall")?{name: { $ne: "All" }}:{},function(err, regions){
		res.json(regions);
	});

});



router.delete('/api/delete/:id', isAuthenticated, havePermission, function(req, res){
	Regions.findByIdAndRemove(req.params.id, (err, region) => {
			  if (err){
					message = 'Echec de suppression de l\'element sélectionné : ' ;
					etat = 'KO';
					log('Delete', 'Echec de suppression du centre: ' + req.params.id, req.user.username);
				}	else {
					log('Delete', 'Suppression du centre : ' + req.params.id, req.user.username);
					message = 'Suppression avec succes de l\'element sélectionné  : ';
					etat = 'OK';
				}
				res.json({ message: message, etat: etat });
   });
});



router.post('/api/add', isAuthenticated, havePermission, function(req, res){

      var regions = new Regions();
      regions.name = req.body.name;
      regions.ville = req.body.ville;
			var etat ='KO';
			var message = '';

      regions.save(function(err) {
          if (err){
							message =err;
							etat ='KO';
						  log('Insert', 'Echec ajout du centre: ' + req.body.ville , req.user.username);
							res.send(err);
						}
          else {
									message ='Opération terminée avec succès';
									etat ='OK';
									log('Insert', 'Ajout d\' une region : ' + req.body.ville , req.user.username);
								  Regions.find(function(err, regions){
									// res.json({ data: entites, message: 'Entite successfully added!' });
									res.json({ data: regions, message: message, etat: etat });
									});
          		}
      });
});



router.post('/api/update/:id', isAuthenticated, havePermission, function(req, res){

		var name = req.body.name;
		var ville = req.body.ville;
		var etat ='KO';
		var message ='';

		Regions.findById(req.params.id, function (err, regions) {
		regions.update({
		name: name,
		ville: ville
	},function (err, regionsID){
    		if(err){
					message =err;
					etat ='KO';
					log('Update', 'Echec modification du centre : ' + req.body.ville ,req.user.username);
    			console.log('GET Error: There was a problem retrieving: ' + err);
    		}else{
					message ='Opération terminée avec succès';
					etat ='OK';
					log('Update', 'Modification du centre : ' + req.body.ville ,req.user.username);
					res.json({ message: message, etat: etat });
				}
			})

		 });

	});





module.exports = router;
