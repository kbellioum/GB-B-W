var express = require('express');
var router = express.Router();
var Regiesparam = require('../models/regiesparam.js');
var Rubriques = require('../models/rubriques.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).param_regies == true){
		return next();
	}else{
		res.redirect('/index');
	}
}
/* GET users listing. */

router.get('/:id', isAuthenticated, havePermission, function(req, res, next) {

	Regiesparam.findById(req.params.id,function(err, regieparamemploi){
		Rubriques.find({'active': true},function(err, rubriques){
		// console.log('koko',projetemploi.length);
		log('Show', 'Consultation de l\'emploi du projet : ' + req.params.id,req.user.username);
		res.render('regieparamemploi', {user: req.user, regieparamemplois: regieparamemploi, rubriques: rubriques, message: req.flash('message') });
	});
});
});

router.get('/api/get/:id',isAuthenticated, havePermission, function(req, res, next) {
	Regiesparam.findById(req.params.id,function(err, regieparam){
			// res.json(exercice);
			res.json(regieparam);
	});

});


router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	Regiesparam.find(function(err, regieparam){
		res.json(regieparam);
	});

});

router.post('/add/:id', isAuthenticated, havePermission, function(req, res, next){
		var etat ='KO';
		var message = '';
	  var programemploi =  req.body.programemploi;

		Regiesparam.findById(req.params.id, function(err, Regiesparam){
			Regiesparam.update({
				emploi: programemploi
			}, function(err, regieparamID){
				if(err){
					message =err;
					etat ='KO';
					log('Insert', 'Echec ajout du credit ouvert de la regie : ' + Regiesparam.ibelle , req.user.username);
					res.send(err);
				}else{
					message ='Opération terminée avec succès';
					etat ='OK';
					log('Insert', 'Ajout credit ouvert de la regie : ' + Regiesparam.ibelle , req.user.username);
					res.redirect('/regiesparam')
				}
			})
		})

})



module.exports = router;
