var express = require('express');
var router = express.Router();

var Vuex = require('vuex');
var Vue = require('vue')

var store = require('store');

store.set('testflag', { state: false });
store.set('inrhflag', { state: false });

store.set('rub', {rubriques: []});

store.set('validerubs', {rubriques: []});
store.set('dispo', {state: false});
store.set('validateflag', {state: false});

Vue.use(Vuex);
var myStore = new Vuex.Store({
	state: {
		flag: false,
		dispo: false,
		validerubs: [],
		rub: []
	},
	mutations: {
		setflag (state, payload) {
			state.flag = payload
		}
	},
	actions: {
		setflag({commit}, payload){
			commit('setflag', payload)
		}
	},
	getters: {
		getflag (state) {
			return state.flag
		},
		getdispo (state) {
			return state.dispo
		},
		getvaliderubs (state) {
			return state.validerubs
		}
	}
})



var Exercices = require('../models/exercices.js');
var Projets = require('../models/projets.js');

var Counter = require('../models/counter.js');
var tool = require("../libs/mylib.js");

const EventEmitter = require('events');



var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}


var core = require('../libs/core.js');
var Core = new core();

// console.log(Core.get());
// console.log(Core.set());
// console.log("la version est : " + Core.getver());

// console.log(Core.getClients());
// console.log(Core.getClients());
// console.log(Core.getClients());

// Variable for the process engagement check
var rub = []
var rubinrh = [];
// var validerubs = [];
// var dispo = false;
var minusrubinrhflag = false;
var minusrubprojflag = false;

class MyActions extends EventEmitter {}

const myActions = new MyActions();

myActions.on('minRubInrh', (item) => {
  console.log('Minus Rubriques INRH Budget');
	return item;
});


myActions.on('minRubProjet', (item) => {
  console.log('Minus Rubriques Projets');
	return "item"
});


myActions.on('transmit', (a, b) => {
	var engage;
	var query = Exercices.find({_id: a})
	var myresult = query.toConstructor();
	return myresult()
	.exec(function(err, result){
		var exercice = result.find(item => item._id == a)
		engage = result[result.indexOf(exercice)].engagements.find(item => item._id == b)
		result[result.indexOf(exercice)].engagements[result[result.indexOf(exercice)].engagements.indexOf(engage)].rubriques
		.forEach(function(item){
			rub.push({rubcode: item.code, ressourceid: item.ressourceid, montant: item.montant})
		});
	});
})


function initModel (exerciceid, engageid) {
	var engage;
	var query = Exercices.find({_id: exerciceid})
	var myresult = query.toConstructor();
	return myresult()
}

function getprojetById (projetid) {
	return Projets.findById(projetid)
	.exec(function(err, projet){
		if(err){
			console.log("Error: ", err)
		}else{
			store.set('projet', {projet: projet})
		}
	})
}


function getprojets () {
	return Projets.find()
	.exec(function(err, projets){
		if(err){
			console.log("Error: ", err)
		}else{
			store.set('projets', {projets: projets})
		}
	})
}

function removeDuplicateOne(propertyName, inputArray) {
  var seenDuplicate = false,
      testObject = {};

  inputArray.map(function(item) {
    var itemPropertyName = item[propertyName];
    if (itemPropertyName in testObject) {
      testObject[itemPropertyName].duplicate = true;
      item.duplicate = true;
      seenDuplicate = true;
    }
    else {
      testObject[itemPropertyName] = item;
      delete item.duplicate;
    }
  });

  // return seenDuplicate;
  return testObject
}

function removeduprub (propertyName, rubsarray){
  var bi = [];

	var tot = removeDuplicateOne(propertyName, rubsarray)

	for(var key in tot){
    var b = rubsarray.filter(item => item.rubcode.trim() == tot[key].rubcode.trim())
    if(b.length > 1){
      bi.push({rubcode: tot[key].rubcode.trim(), ressourceid: tot[key].ressourceid, montant: b.reduce(function(a,b){ return {montant: a.montant + b.montant} }).montant})
    }else{
      bi.push({rubcode: tot[key].rubcode.trim(), ressourceid: tot[key].ressourceid, montant: b[0].montant})
    }
  }
  return bi
}

function getrubdispo (exerciceid, rubs) {
		 var tmp2 = [];
	   return Exercices.findById(exerciceid)
				.exec(function(err, exercice){
					if(err){
						store.set('dispo', {state: false});
					}
					else {
						rubs.forEach(function(rubeng){
							var tmp = exercice.rubriques.find(item => item.code.trim() == rubeng.rubcode.trim() && item.montantdispo >= rubeng.montant )
							if(tmp !== undefined){
							tmp2.push(tmp)
							}
						})
						store.set('validerubs', {rubriques: tmp2})
						store.set('dispo', {state:(rubs.length == store.get('validerubs').rubriques.length)})
					}
				})
}


function getengagerub (exerciceid, engageid) {
	return initModel(exerciceid, engageid)
	.exec(function(err, result){
		var exercice = result.find(item => item._id == exerciceid)
		var engage = result[result.indexOf(exercice)].engagements.find(item => item._id == engageid)
		var tmp = []
		result[result.indexOf(exercice)].engagements[result[result.indexOf(exercice)].engagements.indexOf(engage)].rubriques
		.forEach(function(item){
			tmp.push({rubcode: item.code, ressourceid: item.ressourceid, montant: item.montant})
		});
		store.set('rubs', tmp)
	});
}






function getrubinrh (exerciceid, engageid) {
	return initModel(exerciceid, engageid)
	.exec()
	.then(function(result){
			var exercice = result.find(item => item._id == exerciceid)
			var engage = result[result.indexOf(exercice)].engagements.find(item => item._id == engageid)
			var tmp = []
			result[result.indexOf(exercice)].engagements[result[result.indexOf(exercice)].engagements.indexOf(engage)].rubriques
			.forEach(function(item){
				// if(item.ressourceid.trim() == "59f4627be59c72694683091f"){
				tmp.push({rubcode: item.code, ressourceid: item.ressourceid, montant: item.montant})
				// }
			});
			store.set('rubinrh', {rubriques: tmp});
			console.log("rubinrh: ", store.get('rubinrh'))
		})
	.catch(function(err){
		console.log("Error: ", err);
	})
}


// validate engagements function
// Counter.findOneAndUpdate({name: 'counterengage'}, {$inc: { seq: 1} }, function(error, counter){});
// "EN".concat("00000".substring(0,"00000".length - counter.seq.toString().length), counter.seq)
function validateengage (exerciceid, engageid) {
	return Exercices.findById(exerciceid, function(err, exercice){

		Counter.findOneAndUpdate({exerciceid: exerciceid}, {$inc: { seq_counterengage: 1} }, function(error, counter){

			var engage = exercice.engagements.find(item => item._id == engageid)
			var d = new Date()
			exercice.engagements[exercice.engagements.indexOf(engage)].numeng = "EN".concat("00000".substring(0,"00000".length - counter.seq_counterengage.toString().length), counter.seq_counterengage).concat("/").concat(d.getFullYear())
			exercice.engagements[exercice.engagements.indexOf(engage)].tmpflag = false
			exercice.update({
				 engagements: exercice.engagements
			},function(err, exerciceID){
				if(err){
					console.log("Error: ", err)
				}
				else {
					console.log("Validation de l'engagement avec succès")
				}
			})
		});

	})
	.then(function(err){
		store.set('validateflag', {state: true})
	})
	.catch(function(err){
		store.set('validateflag', {state: false})
	})
}

function checkengage (exerciceid, engageid) {
	return Exercices.findById(exerciceid, function(err, exercice){
		var engage = exercice.engagements.find(item => item._id == engageid)
		store.set('tmpflag', {state: exercice.engagements[exercice.engagements.indexOf(engage)].tmpflag})
	})
}


function getrubs (exerciceid, engageid, ressourceid) {
	return initModel(exerciceid, engageid)
	.exec()
	.then(function(result){
			var exercice = result.find(item => item._id == exerciceid)
			var engage = result[result.indexOf(exercice)].engagements.find(item => item._id == engageid)
			var tmp = []
			result[result.indexOf(exercice)].engagements[result[result.indexOf(exercice)].engagements.indexOf(engage)].rubriques
			.forEach(function(item){
				if(item.ressourceid == ressourceid){
				tmp.push({rubcode: item.code, ressourceid: item.ressourceid, montant: item.montant})
				}
			});
			store.set('projrubs', {rubriques: removeduprub("rubcode", tmp)})
		})
	.catch(function(err){
		console.log("Error: ", err);
	})
}


function getrub (exerciceid, rubcode) {
	return Exercices.findById(exerciceid, function(err, exercice){
		var rub = []
		rub = exercice.rubriques.filter((item) => item.code.trim() == rubcode)
		if(rub.length !== 0){
			store.set('rub', {rubrique: rub})
		}else{
			store.set('rub', {rubrique: null})
		}

	})
}

function getprojrubs (projid) {
	return Projets.find({ _id: projid }).then((projet) => {
		console.log(projet)
		store.set('projrubs', { rubriques: removeduprub( "code", projet[0].emploi )})
	})
}


function minusrubinrh (exerciceid, engageid, rubs) {
	 return Exercices.findById(exerciceid, function(err, exercice){
		// console.log("rubs: " ,rubs);
		rubs.forEach((item) => {
			var tmp = exercice.rubriques.find((r) => r.code.trim() === item.rubcode.trim())
			exercice.rubriques[exercice.rubriques.indexOf(tmp)].montantdispo = exercice.rubriques[exercice.rubriques.indexOf(tmp)].montantdispo - item.montant
// correction par wafik a valider par karim: maj totalengagements in rubrique
			exercice.rubriques[exercice.rubriques.indexOf(tmp)].totalengagements = exercice.rubriques[exercice.rubriques.indexOf(tmp)].totalengagements + item.montant
		})
		exercice.update({
			rubriques: exercice.rubriques
		}, function(err, resultID){
			if(err){
				console.log("Error: ", err)
			}else{
				console.log("Execution avec succes de la fonction minusrubinrh...")
			}
		})

	})
	.then(function(result){
		minusrubinrhflag = true
	})
	.catch(function(err){
		minusrubinrhflag = false
	})
}


function minusrubproj (exerciceid, engageid, projid, rubs) {

	return Projets.findById(projid, function(err, projet){
		console.log("----")
	})
	//  return Exercices.findById(exerciceid, function(err, exercice){
	// 	console.log("rubs: " ,rubs);
	// 	rubs.forEach((item) => {
	// 		var tmp = exercice.rubriques.find( (r) => r.code.trim() === item.rubcode.trim())
	// 		exercice.rubriques[exercice.rubriques.indexOf(tmp)].montantdispo = exercice.rubriques[exercice.rubriques.indexOf(tmp)].montantdispo - item.montant
	// 	})
	// 	exercice.update({
	// 		rubriques: exercice.rubriques
	// 	}, function(err, resultID){
	// 		if(err){
	// 			console.log("Error: ", err)
	// 		}else{
	// 			console.log("Execution avec succes de la fonction minusrubinrh...")
	// 		}
	// 	})
	//
	// })
	// .then(function(result){
	// 	minusrubprojflag = true
	// })
	// .catch(function(err){
	// 	minusrubprojflag = false
	// })
}




/* GET home page. */

router.get('/', isAuthenticated, function(req, res, next) {
  var clients = Core.getClients();
  var tera = "terra mia";
  console.log(tera);
  res.render('mylibs', {user: req.user, tera: tera, clients: clients});
  //res.send("OK OK");
  //res.render('lib', {title: 'lib', clients: "clients"});
});

var RegiesParams = require('../models/regiesparam.js')

router.post('/api/jc', function(req, res, next) {
	RegiesParams.find()
		.then((result) => {
			res.json(result)
		})

})

router.post('/api/get', function(req, res, next) {
	// res.header('Access-Control-Allow-Origin', '*');
	switch (req.body.action) {
		case "minusrubproj":
			minusrubproj(req.body.exerciceid, req.body.engageid, req.body.ressourceid, [1,2,3])
			.then((projet) => {
					// getprojrubs(req.body.ressourceid)
					// 	.then(() => {
					// 		console.log(store.get('projrubs'))
					// 	})
					var tmp = projet.emploi.find(item => item.code.trim() === "21281232")
					projet.emploi[projet.emploi.indexOf(tmp)].disponible = projet.emploi[projet.emploi.indexOf(tmp)].disponible
					console.log("IndexOf tmp: ", projet.emploi.indexOf(tmp))
					console.log(projet.emploi)
					projet.set({ cloturer: true , emploi: projet.emploi })
					projet.save()
						.then(() => res.json({result: "OK", data: projet}))
			})

			break;
		case "engagementByID":
				Exercices.findById(req.body.exerciceid, function(err, exercice){
					var engage = exercice.engagements.filter(item => item._id == req.body.engageid)
					res.json({engagetmp: engage})
				});
			  break;
		case "getrubriquesbyengageid":
				Exercices.findById(req.body.exerciceid, function(err, exercice){
					var tmp = []
					var engage = exercice.engagements.filter(item => item._id == req.body.engageid)
					engage[0].rubriques.forEach(function(data){
						tmp.push(exercice.rubriques.find(item => item.code.trim() == data.code.trim()))
					})
					res.json({rubriques: engage[0].rubriques, rubdispo: tmp})
				});
			  break;
		case "engagements":
		var engage = [];
				Exercices.findById(req.body.exerciceid, function(err, exercice){
					// var engage = exercice.engagements.filter(item => item.tmpflag == true)
 				  // 	version wafik correct exercice empty
					if(exercice !== undefined) { engage = exercice.engagements }
					// res.json({engagetmp: engage})
					// vesrion karim
					// var engage = exercice.engagements
					res.json({result: "OK", engagetmp: engage})
				})
				.catch(function(err){
					res.json({result: "KO", Message: err})
				});
			  break;
		case "getengagements":
		var engage = [];
				Exercices.findById(req.body.exerciceid, function(err, exercice){
					let flag = req.body.flag == 'true'
					// a revoir avec karim : flag avec true ou false ne marche pas,
					// console.log("flag local: ", flag);
					// console.log("flag : ", req.body.flag);
					// console.log("Type flag : ", typeof(req.body.flag));
					//
					// console.log("flag test : ", req.body.flag === false);
					var engage = exercice.engagements.filter(item => item.tmpflag === flag)
					res.json({result: "OK", engagetmp: engage})
				})
				.catch(function(err){
					res.json({result: "KO", Message: err})
				});
			  break;
		case "getrubinrhdispo":

				if(req.body.exerciceid == undefined || req.body.engageid == undefined){
					res.json({result: "KO", Message: "Il faut définir l'un des arguments de l'API {exerciceid ou engageid}"})
					break;
				}
				getrubs(req.body.exerciceid, req.body.engageid, req.body.ressourceid).then(function(data){
						getrubdispo(req.body.exerciceid, store.get('rubinrh').rubriques).then(function(dd){
							if(store.get('dispo').state){
								res.json({result: "OK", Message: "Il y a de la Disponibilité sur la totalité des rubriques"})
							}else{
								res.json({result: "KO", Message: "Il n'y a pas de la Disponibilité sur au moins une des rubriques"})
							}
						})
				})
				rubinrh = []
				validerubs = []
				break;
		case "validaterubinrh":
				console.log("rubinrh: ", store.get('rubinrh'))
				if(req.body.exerciceid == undefined || req.body.engageid == undefined){
					res.json({result: "KO", Message: "Il faut définir l'un des arguments de l'API {exerciceid ou engageid} ou il n'y a pas de rubriques concernant le budget INRH"})
					break;
				}
				getrubinrh(req.body.exerciceid, req.body.engageid).then(function(data){
						// console.log(data);
						console.log("rubinrh: ", store.get('rubinrh'))
						getrubdispo(req.body.exerciceid, store.get('rubinrh').rubriques).then(function(dip){
							// console.log(dip);
							if(store.get('dispo').state){
								minusrubinrh(req.body.exerciceid, req.body.engageid, store.get('rubinrh').rubriques).then(function(){
									console.log("flag: ", minusrubinrhflag)
									if(minusrubinrhflag){
										console.log("Is Here");
										res.json({result: "OK", Message: "Nous avons effectué la mise à jour des rubriques"})
									}else{
										res.json({result: "KO", Message: "Ooops Il y a eu une erreur"})
									}
								})
								.catch(function(err){
									res.json({result: "KO", Message: err})
								});
							}else{
								res.json({result: "KO", Message: "Il n'y a pas de la Disponibilité sur au moins une des rubriques"})
							}
						})
				})
				rubinrh = []
				validerubs = []
				minusrubinrhflag = false
				break;
		case "getengagerub":
				// --------------------------------
				// Cette action renvoi les
				// rubriques et leurs montant
				// au niveau de l'exercice et de
				// l'engagement en question.
				// l'action renvoi aussi l'id de
				// la ressource ou du projet.
				// --------------------------------
				// arguments de l'action
				// exerciceid
				// engageid
				// Action: getengagerub
				// --------------------------------
				// reponse: x le nombre de rubrique
				// {
				//   rubcode: "OK" || "KO"
				//   ressourceid:
				//   montant:
				// }
				// --------------------------------
				// Test OK de l'action
				// --------------------------------
				getengagerub(req.body.exerciceid, req.body.engageid).then(function(data){
					res.json(store.get('rubs'))
				});
				break;
		// case "minusrubinrh":
		// 		getengagerub(req.body.exerciceid, req.body.engageid)
		// 		.then(function(){
		// 			var tmp = store.get('rubs')
		// 			console.log(tmp);
		// 			minusrubinrh(req.body.exerciceid, req.body.engageid, tmp).then(function(err){
		// 				res.json({result: "OK"})
		// 			})
		// 		})
		// 		.catch(function(err){
		// 			res.json({result: "KO", Message: err})
		// 		})
		// 		// rub = []
		// 		break;
		// case "validateengage":
		// 	var result = "NON";
		// 	Exercices.findById(req.body.exerciceid, function(err, exercice){
		// 		var engage = exercice.engagements.find(item => item._id == req.body.engageid)
		// 		exercice.engagements[exercice.engagements.indexOf(engage)].tmpflag = false
		// 		exercice.update({
		// 			 engagements: exercice.engagements
		// 		},function(err, exerciceID){
		// 			if(err){
		// 				res.json({result: "KO", Message: "Engagement non validé"});
		// 			}
		// 			else {
		// 				res.json({result: "OK", Message: "Engagement validé"});
		// 			}
		// 		})
		// 	});
		// 	break;
		case "getrubriquesdispo":
			// --------------------------------
			// Cette action renvoi la
			// disponibilité
			// sur une rubrique pour un
			// montant donné de l'exerciceid
			// l'action renvoi aussi le montant
			// disponible sur la rubrique
			// --------------------------------
			// arguments de l'action
			// exerciceid
			// code
			// montant
			// Action: getrubriquesdispo
			// --------------------------------
			// reponse:
			// {
			//   result: "OK" || "KO"
			//   Message:
			//   MontantDispo:
			// }
			// --------------------------------
			// Test OK de l'action
			// --------------------------------
			var rub = []
			Exercices.findById(req.body.exerciceid)
						.exec()
						.then(function(exercice, err){
							if(err){
								res.json({result: "KO", Message: err, exercice: exercice})
							}
							else {
								rub.push(exercice.rubriques.find(item => item.code.trim() === req.body.code.trim()))

								// Calcul the history of the BC of a rubrique
								var tmp = exercice.engagements.filter(item => item.nature === "BC")
								var tab = []
								var val = 0
								tmp.forEach((item) => {
									item.rubriques.forEach((item2) => {
										if(item2.code.trim() === req.body.code.trim()){
											tab.push(item2)
										}
									})
								})
								if(tab.length !== 0){
									 val = tab.reduce(function(a,b){
										return {montant: a.montant + b.montant}
									}).montant
								}


								// console.log('rub', rub);
								if(rub[0].montantdispo >= req.body.montant){
									res.json({result: "OK", totalmntbc: val, Message: "Montant disponible", MontantDispo: rub[0].montantdispo, budget: rub[0].budget})
								}
								else {
									res.json({result: "KO", totalmntbc: val, Message: "Montant non disponible", MontantDispo: rub[0].montantdispo, budget: rub[0].budget})
								}
							}
						})
					 .catch(function(err){
						 res.json({result: "KO", Message: err})
					 })
			break;
		case "getrub":
			// --------------------------------
			// Cette action renvoi la rubrique
			// demandée via son code depuis
			// l'exercice en question
			// --------------------------------
			// arguments de l'action
			// exerciceid
			// code
			// Action: getrub
			// --------------------------------
			// Test OK de l'action
			// --------------------------------
			getrub(req.body.exerciceid, req.body.code).then(function(){
				var tt = store.get('rub')
				if(tt.rubrique == null){
					res.json({result: "KO", Message: "La rubrique n'existe pas"})
				}else{
					res.json({result: "OK", Message: "La rubrique existe bien", rubrique: tt.rubrique})
				}
			})
			break;
		case "validateengage":
			// --------------------------------
			// Cette action valide l'engagement
			// qui ne comporte que les rubriques
			// relatives au budget INRH
			// --------------------------------
			// arguments de l'action
			// exerciceid
			// engageid
			// Action: validateengage
			// --------------------------------
			// Test OK de l'action
			// --------------------------------
			checkengage(req.body.exerciceid, req.body.engageid)
			.then(function(result, err){
				if(store.get('tmpflag').state){
					getrubinrh(req.body.exerciceid, req.body.engageid).then(function(){
						var tt = store.get('rubinrh')
						// if(tt.length == 0 ){ console.log("Ooops rubriques vide") }else{ console.log("tt.length: ", tt.length) }
						getrubdispo(req.body.exerciceid, tt.rubriques).then(function(err){
							if(store.get('dispo').state){
								minusrubinrh(req.body.exerciceid, req.body.engageid, tt.rubriques).then(function(err){
									validateengage(req.body.exerciceid, req.body.engageid).then(function(err){
										console.log("validateflag: ", store.get('validateflag').state)
										if(store.get('validateflag').state){
											res.json({result: "OK", validerubs: store.get('validerubs'), message: 'Engagement validé avec succes'})
										}else{
											res.json({result: "KO", validerubs: store.get('validerubs'), message: 'Ooops il y a un problème'})
										}
									})
								})
							}else{
								res.json({result: "KO", validerubs: store.get('validerubs')})
							}
						})

					})
				}else{
					res.json({result: "KO", Message: "L'engagement est déjà validé . . ."})
				}
			})
			break;
		case "test":
			checkengage(req.body.exerciceid, req.body.engageid)
			.then(function(result, err){
				if(store.get('tmpflag').state){
					getrubinrh(req.body.exerciceid, req.body.engageid).then(function(){
						var tt = store.get('rubinrh')
						getrubdispo(req.body.exerciceid, tt.rubriques).then(function(err){
							if(store.get('dispo').state){
								minusrubinrh(req.body.exerciceid, req.body.engageid, tt.rubriques).then(function(err){
									validateengage(req.body.exerciceid, req.body.engageid).then(function(err){
										console.log("validateflag: ", store.get('validateflag').state)
										if(store.get('validateflag').state){
											res.json({result: "OK", validerubs: store.get('validerubs'), message: 'Engagement validé avec succes'})
										}else{
											res.json({result: "KO", validerubs: store.get('validerubs'), message: 'Ooops il y a un problème'})
										}
									})
								})
							}else{
								res.json({result: "KO", validerubs: store.get('validerubs'), Message: "Pas de disponibilité"})
							}
						})

					})
				}else{
					res.json({result: "KO", Message: "L'engagement est déjà validé . . ."})
				}
			})
			break;
		case "test2":
			getrubs(req.body.exerciceid, req.body.engageid, req.body.ressourceid)
			.then(function(err){
				getrubdispo(req.body.exerciceid, store.get('projrubs').rubriques)
				.then(function(err){
					getprojetById(req.body.ressourceid)
					.then(function(err){
						getprojets().then(function(err){
							res.json({ projets: store.get('projets').projets ,projet: store.get('projet').projet.emploi, rubriques: store.get('projrubs').rubriques, dispo: store.get('dispo').state});
						})
					})

				})

			});
			store.set('dispo', {state: false})
			break;
		case "getrubproj":
			Projets.find({ _id: req.body.projid })
				.then((projet) => {
					res.json({result: "OK", rubriques: projet[0].emploi})
				})
			break;
		case "getrubprojdispo":
			let tmp = {};
			let flag = [];
			let keys = [];
			let rubriquesProjets = [];
			let proj;
			Exercices.findOne({ _id: req.body.exerciceid})
			.then((exercice) => {
				Projets.find({})
				.then((projets) => {
					let engage = exercice.engagements.find(item => item._id == req.body.engageid)
					let obj = removeDuplicateOne('ressourceid', engage.rubriques)
					keys = Object.keys(obj)
					let inrhbdg = keys.find(item => item.trim() === '59f4627be59c72694683091f '.trim())
					keys.splice(keys.indexOf(inrhbdg),1)
					// console.log(keys)
					keys.forEach(key => {
						tmp[key.trim()] = engage.rubriques.filter(item => item.ressourceid.trim() == key.trim())
						tmp[key.trim()].forEach( key => {
							let proj = projets.find(item => item._id == key.ressourceid.trim())
							let rubin = proj.emploi.find(item => item.code.trim() === key.code.trim())
							if(rubin.disponible >= key.montant){
								flag.push(true)
							}else{
								flag.push(false)
								rubriquesProjets.push(rubin)
								// console.log("rubin in false: ", rubin, key)
							}
							// console.log("------------------------")
							// console.log("rubin: ", rubin)
							// console.log("------------------------")
							// console.log(key)
							// console.log(proj.emploi)
							// console.log("FLAG =============== > ", flag)

						})
						// console.log("------------------------")
						// let proj = projets.find(item => item._id == tmp[key.trim()].ressourceid)
						// console.log(proj)
					})
					// console.log("tmp: ", tmp["59f4627be59c72694683091f"])
					console.log("dispo result: ",flag.find(item => item === false) === undefined? true: flag.find(item => item === false))
					if(flag.find(item => item === false) === undefined? true: false){
						res.json({result: "OK", message: "Il y a de la dispo sur les rubriques projet", rubriques: engage.rubriques, tmp: tmp, projetsid: keys, rubriquesProjets: rubriquesProjets})
					// 	keys.forEach(key => {
					// 		tmp[key.trim()] = engage.rubriques.filter(item => item.ressourceid.trim() == key.trim())
					// 		tmp[key.trim()].forEach(key => {
					// 			proj = projets.find(item => item._id == key.ressourceid.trim())
					// 			let rubin = proj.emploi.find(item => item.code.trim() === key.code.trim())
					// 			projets.emploi[proj.emploi.indexOf(rubin)].disponible = projets.emploi[proj.emploi.indexOf(rubin)].disponible - key.montant
					// 			console.log("Rubrique: ", projets.emploi[proj.emploi.indexOf(rubin)].disponible)
					// 			console.log("Montant", key.montant)
					// 		})
					// 	})
						// console.log(tmp)
						// console.log(keys)
						// proj.set({
						// 	emploi: proj.emploi
						// }, function(err, resultID){
						// 	if(err){
						// 		console.log("Error: ", err)
							}else{
								res.json({result: "KO", Message: "Il n'y a pas de dispo sur les rubriques projets", rubriques: engage.rubriques, tmp: tmp, projetsid: keys, rubriquesProjets: rubriquesProjets})
						// 		// console.log("Execution avec succes de la fonction minusrubinrh...")
							}
						// })



					// }else{
					//
					// }

				})
			})
			break;
		case "test3":
			let tmp3 = {};
			let flag3 = [];
			let keys3 = [];
			let rubin;
			let proj3;
			Exercices.findOne({ _id: req.body.exerciceid})
			.then((exercice) => {
				Projets.find({})
				.then((projets) => {
					let engage = exercice.engagements.find(item => item._id == req.body.engageid)
					let obj = removeDuplicateOne('ressourceid', engage.rubriques)
					keys3 = Object.keys(obj)
					let inrhbdg = keys3.find(item => item.trim() === '59f4627be59c72694683091f '.trim())
					keys3.splice(keys3.indexOf(inrhbdg),1)
					// console.log(keys)
					keys3.forEach(key => {
						tmp3[key.trim()] = engage.rubriques.filter(item => item.ressourceid.trim() == key.trim())
						tmp3[key.trim()].forEach(key => {
							let proj = projets.find(item => item._id == key.ressourceid.trim())
							let rubin = proj.emploi.find(item => item.code.trim() === key.code.trim())
							if(rubin.disponible >= key.montant){
								flag3.push(true)
							}else{
								flag3.push(false)
								// console.log("rubin in false: ", rubin, key)
							}
							// console.log("------------------------")
							// console.log("rubin: ", rubin)
							// console.log("------------------------")
							// console.log(key)
							// console.log(proj.emploi)
							// console.log("FLAG =============== > ", flag)

						})
						// console.log("------------------------")
						// let proj = projets.find(item => item._id == tmp[key.trim()].ressourceid)
						// console.log(proj)
					})
					// console.log("tmp: ", tmp["59f4627be59c72694683091f"])
					console.log("dispo result: ",flag3.find(item => item === false) === undefined? true: flag3.find(item => item === false))
					if(flag3.find(item => item === false) === undefined? true: false){
						keys3.forEach(key => {
							tmp3[key.trim()] = engage.rubriques.filter(item => item.ressourceid.trim() == key.trim())
							tmp3[key.trim()].forEach(key => {
								proj3 = projets.find(item => item._id == key.ressourceid.trim())
								rubin = proj3.emploi.find(item => item.code.trim() === key.code.trim())
								projets[projets.indexOf(proj3)].emploi[proj3.emploi.indexOf(rubin)].disponible = projets[projets.indexOf(proj3)].emploi[proj3.emploi.indexOf(rubin)].disponible - key.montant
								// console.log("Rubrique: ", projets)
								rubin.set({
									disponible: rubin.disponible - key.montant
								})

								console.log("RubIn: ", rubin)

								console.log("Montant", key.montant)
							})
						})
						console.log("Projet: ", projets)

						proj3.save()
						.then(() => {
							res.json({result: "OK", message: "Mise à jour des rubriques projets OK", rubriques: engage.rubriques, tmp: tmp3, projetsid: keys3, projets: projets})
						})
						.catch((err) => {
							res.json({result: "KO", message: err})
						})
						// console.log(tmp)
						// console.log(keys)
						// proj.set({
						// 	emploi: proj.emploi
						// }, function(err, resultID){
						// 	if(err){
						// 		console.log("Error: ", err)
						// 	}else{

								// console.log("Execution avec succes de la fonction minusrubinrh...")
						// 	}
						// })



					}else{
						res.json({result: "KO", rubriques: engage.rubriques, tmp: tmp3, projetsid: keys3, projets: projets})
					}

				})
			})
			break;
		default:
			res.json({result: "KO", Message: "L'action n'est pas correcte"})
	}

});
// myStore.dispatch('setflag', true)
// res.json({flag: myStore.getters.getflag, validerubs: myStore.getters.getvaliderubs, dispo: myStore.getters.getdispo})

module.exports = router;
