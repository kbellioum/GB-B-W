var express = require('express');
var router = express.Router();
var Centres = require('../models/centres.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");

var message ='';
var etat ='KO';

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).param_centres== true){
		return next();
	}else{
		res.redirect('/index');
	}
}
/* GET users listing. = param_entitebenef =*/

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Centres.find(function(err, centres){
		log('Show', 'Consultation de la liste des centres.',req.user.username);
		res.render('centres', {user: req.user, centres: centres, message: req.flash('message') });
	});

});

router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	Centres.find(function(err, centres){
		res.json(centres);
	});

});

router.delete('/api/delete/:id',isAuthenticated, havePermission, function(req, res){
	Centres.findByIdAndRemove(req.params.id, (err, centre) => {
			  if (err){
					message = 'Echec de suppression de l\'element sélectionné : ' ;
					etat = 'KO';
					log('Delete', 'Echec de suppression du centre: ' + req.params.id,req.user.username);
				}	else {
					log('Delete', 'Suppression du centre : ' + req.params.id,req.user.username);
					message = 'Suppression avec succes de l\'element sélectionné  : ';
					etat = 'OK';
				}
				res.json({ message: message, etat: etat });
   });
});

router.post('/api/add',isAuthenticated, havePermission, function(req, res){

      var centres = new Centres();
      centres.code = req.body.code;
      centres.libelle = req.body.libelle;
			var etat ='KO';
			var message = '';

      centres.save(function(err) {
          if (err){
							message =err;
							etat ='KO';
						  log('Insert', 'Echec ajout du centre: ' + req.body.libelle ,req.user.username);
							res.send(err);
						}
          else {
									message ='Opération terminée avec succès';
									etat ='OK';
								  Centres.find(function(err, centres){
									log('Insert', 'Ajout du centre : ' + req.body.libelle ,req.user.username);
									// res.json({ data: entites, message: 'Entite successfully added!' });
									res.json({ data: centres, message: message, etat: etat });
									});
          		}
      });
});

router.post('/api/update/:id',isAuthenticated, havePermission, function(req, res){

		var code = req.body.code;
		var libelle = req.body.libelle;
		var etat ='KO';
		var message ='';

		Centres.findById(req.params.id, function (err, centres) {
		centres.update({
		code: code,
		libelle: libelle
	},function (err, centresID){
    		if(err){
					message =err;
					etat ='KO';
					log('Update', 'Echec modification du centre : ' + req.body.libelle ,req.user.username);
    			console.log('GET Error: There was a problem retrieving: ' + err);
    		}else{
					message ='Opération terminée avec succès';
					etat ='OK';
					log('Update', 'Modification du centre : ' + req.body.libelle ,req.user.username);
					res.json({ message: message, etat: etat });
				}
			})

		 });

	});





module.exports = router;
