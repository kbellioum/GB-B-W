var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var Comptesbancaires = require('../models/comptesbancaires.js');
var Sourcesbudget = require('../models/sourcesbudget.js');
var Debiteurs = require('../models/debiteurs.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).etat_recettes == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

/* GET users listing.  = etat_recettes = */


router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Exercices.find({ "disabled" : false }, { _id: 1,libelle: 1 },function(err, exercices){
		  	Comptesbancaires.find(function(err, comptesbancaires){
								Sourcesbudget.find(function(err, sourcesbudget){
									Debiteurs.find(function(err, debiteurs){
										res.render('recetteform', {user: req.user,exercices: exercices,comptesbancaires: comptesbancaires,sourcesbudget: sourcesbudget,debiteurs: debiteurs,  message: req.flash('message') });
								});
						});		
				});
	}).sort( { createddate: -1 } );
});


router.post('/api/add/:id',isAuthenticated, havePermission, function(req, res){

	var comptebancaireid = req.params.id;
	var numavis = 00001;
	var exercice = req.body.exercices;
	var dateav = new Date(req.body.dateavis).toLocaleString(['fr-FR'], { day: '2-digit', month: '2-digit', year: 'numeric'});
	var debiteur = req.body.debiteur;
	var objet = req.body.objet;
	var montant = Number(req.body.montant);
	var montantlettre = "UN DEUX TROIS";
	var dateencaissement =  req.body.dateencaissement;
	var sourcebudget = req.body.sourcebudget;
  var justif = req.body.justif;
  var comptagenerale = [];
	var ecriture = {};

	Comptesbancaires.findById(comptebancaireid,function(err, comptesbancaires){
		code = comptesbancaires.code;
		ecriture = {
			numavis: numavis,
			exercice: exercice,
			dateav:  dateav,
			debiteur: debiteur,
			objet: objet,
			montant: montant,
			montantlettre: montantlettre,
			dateencaissement: dateencaissement,
			sourcebudget: sourcebudget,
			justif: justif,
			comptagenerale: comptagenerale,
			datesaisie: js_yyyy_mm_dd_hh_mm_ss()
			};
			comptesbancaires.ecritures.push(ecriture);

			comptesbancaires.update({
				ecritures: comptesbancaires.ecritures,
				solde: comptesbancaires.solde + montant
			 },function (err, comptebancaireID){
				 if(err){
					 log('Insert', 'Echec ajout d\'une recette d\'un montant : ' + montant + ' au compte bancaire '+ code ,req.user.username);
					 console.log('GET Error: There was a problem retrieving: ' + err);
				 }else{
					 log('Insert', 'Ajout d\'une recette d\'un montant : ' + montant + ' au compte bancaire '+ code ,req.user.username);
					res.redirect('/recettes');
				 }
			 });

		});

});






	function js_yyyy_mm_dd_hh_mm_ss () {
		now = new Date();
		year = "" + now.getFullYear();
		month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
		day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
		hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
		minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
		second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
		return day + "/" + month + "/" + year + "  " + hour + ":" + minute + ":" + second;
	}

module.exports = router;
