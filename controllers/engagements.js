var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var EngageTmp = require('../models/engagetmp.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).dep_engage == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	var filter = {}
	if(req.user.region.name === "All"){
		filter = {clotured: false, disabled: false}
	}else{
		filter = {region: req.user.region._id, clotured: false}
	}
		Exercices.find(filter,{ _id: 1,libelle: 1, clotured: 1, disabled: 1}, function(err, exercices){
			log('Show', 'Consultation de la liste des engagements.',req.user.username);
			res.render('engagements', {user: req.user, exercices: exercices, message: req.flash('message') });
		});
});

router.get('/api/get/:id',isAuthenticated, havePermission, function(req, res, next) {
	Exercices.findById(req.params.id,function(err,exercice){
		res.json(exercice.engagements);
	});
});

router.delete('/api/delete/:id', isAuthenticated, havePermission, function(req, res){
	var etat ='KO';
	var message ='';
	Exercices.findOneAndUpdate({'engagements._id':req.params.id},
			{
			  $pull: { engagements: {_id:req.params.id}}
			},{new:true},
			function(err, engagement) {
			  if(err){
					message = 'Echec de suppression de l\'element sélectionné : ' ;
					etat = 'KO';
					log('Delete', 'Echec de suppression de l\'engagement : ' + req.params.id,req.user.username);
				}else{
					message = 'Suppression avec succes de l\'element sélectionné';
					etat = 'OK';
					log('Delete', 'Suppression de l\'engagement : ' + req.params.id,req.user.username);
				}
				console.log('etat final : ', etat);
				res.json({ message: message, etat: etat });
			})

});


module.exports = router;
