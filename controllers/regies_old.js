var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');

var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).dep_engage == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

router.post('/', function(req, res, next) {
	Exercices.findOne({ _id: req.body.exerciceid })
		.then((exercice) => {
			res.json({result: "OK", regies: exercice.regies})
		})
});

router.post('/add', function(req, res, next) {
	var regie = {
		libelle: req.body.libelle,
		responsable: req.body.userid

	}
	Exercices.find({ _id: req.body.exerciceid })
		.then((exercice) => {
			var regieexer = exercice.find(item => item._id == req.body.exerciceid)
			regieexer.regies.push(regie)
			regieexer.save()
				.then(() => {
					res.json({result: "OK", exercice: regieexer, regie: regie})
				})
		})
});


router.post('/delete', function(req, res, next) {
	Exercices.findOne({ _id: req.body.exerciceid })
		.then((exercice) => {
			// var regieexer = exercice.filter(item => item._id == req.body.exerciceid)
			var regie = exercice.regies.find(item => item._id == req.body.regieid)
			regie.remove()
				.then(() => {
					exercice.save()
						.then(() => {
							res.json({result: "OK", exercice: exercice, regie: regie})
						})
				})
		})
});


router.post('/update', function(req, res, next) {
	Exercices.findOne({ _id: req.body.exerciceid })
		.then((exercice) => {
			// var regieexer = exercice.filter(item => item._id == req.body.exerciceid)
			var regie = exercice.regies.find(item => item._id == req.body.regieid)
			regie.set({
				libelle: req.body.libelle,
				responsable: req.body.userid,
				regieprog: req.body.obj
			})
			exercice.save()
			 .then(() => {
				 res.json({result: "OK", exercice: exercice, regie: regie})
			 })
		})
});






module.exports = router;
