var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var EngageTmp = require('../models/engagetmp.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).regie_alimcaisse == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	var filter = {}
	if(req.user.region.name === "All"){
		filter = {clotured: false}
	}else{
		filter = {region: req.user.region._id, clotured: false}
	}
		Exercices.find(filter,{ _id: 1,libelle: 1 }, function(err, exercices){
			log('Show', 'Consultation de la liste des engagements.',req.user.username);
			res.render('regiescaisse', {user: req.user, exercices: exercices, message: req.flash('message') });
		});
});
// ,isAuthenticated, havePermission
router.get('/api/get/:id', function(req, res, next) {
	Exercices.findById(req.params.id,function(err,exercice){
		var obj = {}
		var tb = []
		if(exercice.regies.length > 0){
			exercice.regies.forEach(item => {
				// Si user est regisseur ==> filtrer par regies (chaque regisseur affiche seulement sa regie) Sinon afficher toutes les regies de la region de l'user
				if(req.user.regisseur){
					if(String(item.regisseur) === String(req.user._id)){
						obj = {
							_id: item._id,
							libelle: item.libelle,
							regisseur:item.regisseur
						}
						tb.push(obj)
					}

				}else {
					obj = {
						_id: item._id,
						libelle: item.libelle,
						regisseur:item.regisseur
					}
					tb.push(obj)
				}



			})
		}

		res.json(tb);
	});
});

router.get('/api/getcaisse/:id', function(req, res, next) {
	var exerciceid = req.params.id.split('|')[0]
	var regieid = req.params.id.split('|')[1]

	if(regieid !== "vide" && exerciceid !== "vide"){


		Exercices.findById({_id: exerciceid})
			.then((data) => {
				var regie = data.regies.find(item => item._id == regieid)
				var totaux = gettotal_encaiss_regie (regie)
				res.json({result: "OK", regid: regieid, caisse: regie.caisse, totaux: totaux});
				// res.json(data.regies);
			})
	}


});

function gettotal_encaiss_regie (regie) {
				var total_credit = 0;
				var total_debit = 0;
			// calcul total encaissements
				for (var j=0; j < regie.caisse.length; j++){
					total_credit += regie.caisse[j].montant;
				}
				// calcul total depenses
				for (var i=0; i < regie.journauxdecaisse.length; i++){
					// calcul somme des depenses detail jc
									if(regie.journauxdecaisse[i].detail.length === 0 ){
										var totaldepense = {montant:0};
									} else {
										 var totaldepense = regie.journauxdecaisse[i].detail.reduce(function(a,b){
												return {montant: a.montant + b.montant}
											})
									}
						total_debit += totaldepense.montant;
					}
					return {"total_credit":total_credit, "total_debit": total_debit}
}


router.get('/api/getcreditouvert/:id', function(req, res, next) {
	var exerciceid = req.params.id.split('|')[0]
	var regieid = req.params.id.split('|')[1]

	if(regieid !== "vide" && exerciceid !== "vide"){
		Exercices.findById({_id: exerciceid})
			.then((data) => {
				var regie = data.regies.find(item => item._id == regieid)
				res.json({result: "OK", regid: regieid, emploi: regie.emploi});

				// res.json(data.regies);
			})
	}


});

router.post('/api/caisse/add', function(req, res, next) {
	var exerciceid = req.body.exerciceid;
	var regieid = req.body.regieid;
	var caisse = req.body.caisse;


	Exercices.findById({_id: exerciceid})
		.then((data) => {
			var regie = data.regies.find(item => item._id == regieid)
			regie.caisse.push(caisse)
			data.save()
			.then(function(err){
				res.json({result: "OK", message: "Opération réussie",caisse: regie.caisse })
			})
			.catch(function(err){console.log("error",err);
				res.json({result: "KO", message: "Il y a eu un problème" })
			})
		})

});

router.post('/api/caisse/remove', function(req, res, next) {
	var exerciceid = req.body.exerciceid;
	var regieid = req.body.regieid;
	var caisseelmid = req.body.caisseelmid;

	Exercices.findById({_id: exerciceid})
		.then((data) => {
			var regie = data.regies.find(item => item._id == regieid)
			var element = regie.caisse.find(item => item._id == caisseelmid)
			if(element === undefined){
				res.json({result: "KO", message: "l'element n'existe pas"});
			}else{
				element.remove()
					.then(() => {
						data.save()
							.then(() => {
								res.json({result: "OK", message: "Opération réussie", elem: element});
							})
					})
			}

			// res.json(data.regies);
		})

});






router.delete('/api/delete/:id', isAuthenticated, havePermission, function(req, res){
	var etat ='KO';
	var message ='';
	Exercices.findOneAndUpdate({'engagements._id':req.params.id},
			{
			  $pull: { engagements: {_id:req.params.id}}
			},{new:true},
			function(err, engagement) {
			  if(err){
					message = 'Echec de suppression de l\'element sélectionné : ' ;
					etat = 'KO';
					log('Delete', 'Echec de suppression de l\'engagement : ' + req.params.id,req.user.username);
				}else{
					message = 'Suppression avec succes de l\'element sélectionné';
					etat = 'OK';
					log('Delete', 'Suppression de l\'engagement : ' + req.params.id,req.user.username);
				}
				res.json({ message: message, etat: etat });
			})

});


module.exports = router;
