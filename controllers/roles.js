var express = require('express');
var router = express.Router();
var Roles = require('../models/roles.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");



var Permissions = require('../libs/permissions.js');

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).adm_role == true){
		return next();
	}else{
		res.redirect('/index');
	}
}




/* GET users listing. */

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Roles.find(function(err, roles){
		log('Show', 'Consultation de la liste des rôles.',req.user.username);
		res.render('roles', {user: req.user, roles: roles, message: req.flash('message') });
	});

});

router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	Roles.find(function(err, roles){
		res.json(roles);
	});

});

router.get('/api/get/:id', isAuthenticated, havePermission, function(req, res, next) {
	Roles.findById(req.params.id, function(err, roles){
		res.json(roles);
	});

});

router.delete('/api/delete/:id',isAuthenticated, havePermission, function(req, res){
	var etat ='KO';
	var message = '';
	Roles.remove({
		_id: req.params.id
	}, function(err, role) {
		if (err){
			message = 'Echec de suppression de l\'element sélectionné : ' ;
			etat = 'KO';
			log('Delete', 'Echec de suppression du rôle  : ' + req.params.id,req.user.username);

		}	else {
			log('Delete', 'Suppression du rôle  : ' + req.params.id,req.user.username);
			message = 'Suppression avec succes de l\'element sélectionné  : ';
			etat = 'OK';
		}
		res.json({ message: message, etat: etat });
	});
});

router.post('/api/add',isAuthenticated, havePermission, function(req, res){
 var roles = new Roles();
 roles.nomrole = req.body.nomrole;
	var etat ='KO';
	var message = '';

	roles.save(function(err) {
			if (err){
					message =err;
					etat ='KO';
					log('Insert', 'Echec ajout du rôle : ' + req.body.libelle ,req.user.username);
					res.send(err);
				}
			else {
							message ='Opération terminée avec succès';
							etat ='OK';
							Roles.find(function(err, roles){
							log('Insert', 'Ajout du rôle : ' + req.body.libelle ,req.user.username);
							// res.json({ data: entites, message: 'Entite successfully added!' });
							res.json({ data: roles, message: message, etat: etat });
							});
					}
	});
});

router.post('/api/update/:id', isAuthenticated, havePermission, function(req, res){
	var nomrole = req.body.nomrole;
	var etat ='KO';
	var message ='';

	Roles.findById(req.params.id, function (err, roles) {
		roles.update({
				nomrole: nomrole
	},function (err, rolesID){
			if(err){
				message =err;
				etat ='KO';
				log('Update', 'Echec modification du rôle : ' + req.body.libelle ,req.user.username);
				console.log('GET Error: There was a problem retrieving: ' + err);
			}else{
				message ='Opération terminée avec succès';
				etat ='OK';
				log('Update', 'Modification du rôle : ' + req.body.libelle ,req.user.username);
				res.json({ message: message, etat: etat });
			}
		})

	 });
	});





module.exports = router;
