var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).exer_vircrd == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

/* GET users listing. = exer_vircrd = */

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	var filter = {}
	if(req.user.region.name === "All"){
		filter = {clotured: false}
	}else{
		filter = {region: req.user.region._id, clotured: false}
	}
	Exercices.find(filter, { _id: 1,libelle: 1 },function(err, exercices){
		log('Show', 'Consultation de la liste des virements de credit.',req.user.username);
		res.render('virementscredits', {user: req.user, exercices: exercices, message: req.flash('message') });
	}).sort( { createddate: -1 } );

});

router.get('/api/get/:id',isAuthenticated, havePermission, function(req, res, next) {
	Exercices.findById(req.params.id,function(err, exercice){
		res.json(exercice.virementscredits);
	});

});


	function js_yyyy_mm_dd_hh_mm_ss () {
		now = new Date();
		year = "" + now.getFullYear();
		month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
		day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
		hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
		minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
		second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
		return day + "/" + month + "/" + year + "  " + hour + ":" + minute + ":" + second;
	}

module.exports = router;
