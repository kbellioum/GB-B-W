var express = require('express');
var router = express.Router();
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");
var Exercices = require('../models/exercices.js');

var message ='';
var etat ='KO';

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).param_entitebenef == true){
		return next();
	}else{
		res.redirect('/index');
	}
}
/* GET users listing. = param_entitebenef =*/

router.get('/', isAuthenticated, havePermission, function(req, res, next) {

		res.render('ordresvirement', {user: req.user,  message: req.flash('message') });

});

router.get('/:id', isAuthenticated, havePermission, function(req, res, next) {

		var exerciceid = req.params.id.split('|')[1]
		var engageid = req.params.id.split('|')[2]
		var opid = req.params.id.split('|')[3]
		var typeregl = req.params.id.split('|')[0]
		
		Exercices.findOne({_id: exerciceid})
		.populate('engagements.beneficiaireid')
		.then((data) => {
			var tmpeng = data.engagements.find(item => item._id == engageid)
			var tmpop = tmpeng.op.find(item => item._id == opid)

			res.render('ordresvirement', {user: req.user,  dataengage: tmpeng, dataop: tmpop, typeregl: typeregl, message: req.flash('message') });
		})


});











// router.get('/get/:id', isAuthenticated, havePermission, function(req, res, next){
//
// 	var exerciceid = req.params.id.split('|')[0]
// 	var engageid = req.params.id.split('|')[1]
// 	// var opid = req.params.split('|')[2]
//
// 	Exercices.findById(exerciceid, function(err, exercice){
// 		var engage = exercice.engagements.find(item => item._id == engageid)
// 		res.json({result: "OK", exerciceid: exerciceid, engageid: engageid, engagement: engage})
// 	})
//
// })



module.exports = router;
