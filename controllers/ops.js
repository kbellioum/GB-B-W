var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var Counter = require('../models/counter.js');

var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).dep_engage == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	var filter = {}
	if(req.user.region.name === "All"){
		filter = {clotured: false, disabled: false}
	}else{
		filter = {region: req.user.region._id, clotured: false, disabled: false}
	}
		Exercices.find(filter,{ _id: 1,libelle: 1 }, function(err, exercices){
			log('Show', 'Consultation de la liste des ordres de paiement.',req.user.username);
			res.render('ops', {user: req.user, exercices: exercices, message: req.flash('message') });
		});
});




router.get('/api/get/:id', isAuthenticated, havePermission, function(req, res, next) {
var tabop = [];
	Exercices.findById(req.params.id,function(err,exercice){
				for (var i=0; i < exercice.engagements.length; i++){
					for (var j=0; j < exercice.engagements[i].op.length; j++){
							tabop.push(
								{				"opid": exercice.engagements[i].op[j]._id,
								 				"engageid": exercice.engagements[i]._id,
												"exerciceid": exercice._id,
								 				"numeng": exercice.engagements[i].numeng,
												"beneficiaire": exercice.engagements[i].beneficiaire,
												"nature": exercice.engagements[i].nature,
												"numop": exercice.engagements[i].op[j].numop,
												"dateop": exercice.engagements[i].op[j].dateop,
												"objetop": exercice.engagements[i].op[j].objetop,
												"montantop": exercice.engagements[i].op[j].montantop,
												"tmpflagop": exercice.engagements[i].op[j].tmpflagop,
												"lastupdate": exercice.engagements[i].op[j].lastupdate
								}
						  )
				}}
				// console.log(tabop);
		res.json(tabop);
	});
});
router.get('/api/get_opvalide/:id', isAuthenticated, havePermission, function(req, res, next) {
	var tabop = [];
	Exercices.findById(req.params.id,function(err,exercice){
				for (var i=0; i < exercice.engagements.length; i++){
					for (var j=0; j < exercice.engagements[i].op.length; j++){
								if(exercice.engagements[i].op[j].tmpflagop === false){
									tabop.push(
										{				"opid": exercice.engagements[i].op[j]._id,
														"engageid": exercice.engagements[i]._id,
														"exerciceid": exercice._id,
														"numeng": exercice.engagements[i].numeng,
														"beneficiaire": exercice.engagements[i].beneficiaire,
														"nature": exercice.engagements[i].nature,
														"nummarche": exercice.engagements[i].nummarche,
														"numbc": exercice.engagements[i].numbc,
														"datebc": exercice.engagements[i].datebc,
														"autreinfos": exercice.engagements[i].autreinfos,
														"numop": exercice.engagements[i].op[j].numop,
														"dateop": exercice.engagements[i].op[j].dateop,
														"objetop": exercice.engagements[i].op[j].objetop,
														"montantop": exercice.engagements[i].op[j].montantop,
														"modereglement": exercice.engagements[i].op[j].modereglement,
														"numreglement": exercice.engagements[i].op[j].numreglement,
														"datereglement": exercice.engagements[i].op[j].datereglement,
														"datevisa": exercice.engagements[i].op[j].datevisa,
														"tmpflagop": exercice.engagements[i].op[j].tmpflagop,
														"visaflagop": exercice.engagements[i].op[j].visaflagop,
														"regflagop": exercice.engagements[i].op[j].regflagop,
														"lastupdate": exercice.engagements[i].op[j].lastupdate,
														"ovtyperegl": exercice.engagements[i].op[j].ovtyperegl,
														"ovdevise": exercice.engagements[i].op[j].ovdevise,
														"annuler": exercice.engagements[i].op[j].annuler
										}
									)
								}

				}}
		res.json(tabop);
	});
});
router.get('/api/get_op_regflagop/:id', isAuthenticated, havePermission, function(req, res, next) {
	var tabop = [];
	Exercices.findById(req.params.id,function(err,exercice){
				for (var i=0; i < exercice.engagements.length; i++){
					for (var j=0; j < exercice.engagements[i].op.length; j++){
								if(exercice.engagements[i].op[j].regflagop === true){
									tabop.push(
										{				"opid": exercice.engagements[i].op[j]._id,
														"engageid": exercice.engagements[i]._id,
														"exerciceid": exercice._id,
														"numeng": exercice.engagements[i].numeng,
														"beneficiaire": exercice.engagements[i].beneficiaire,
														"numop": exercice.engagements[i].op[j].numop,
														"dateop": exercice.engagements[i].op[j].dateop,
														"montantop": exercice.engagements[i].op[j].montantop,
														"lastupdate": exercice.engagements[i].op[j].lastupdate,
														"annuler": exercice.engagements[i].op[j].annuler,
														"numjournal": exercice.engagements[i].op[j].numjournal,
														"numpiece": exercice.engagements[i].op[j].numpiece,
														"typecharge": exercice.engagements[i].op[j].typecharge,
														"comptaflagop": exercice.engagements[i].op[j].comptaflagop
										}
									)
								}

				}}
				console.log("tabop is", tabop);
		res.json(tabop);
	});
});


router.get('/api/get/', isAuthenticated, havePermission, function(req, res, next) {
			var tabop = [];
			res.json(tabop);
 });

// router.delete('/api/delete/:id', isAuthenticated, havePermission, function(req, res){
// 	var etat ='KO';
// 	var message ='';
// 	Exercices.findOneAndUpdate({'engagements._id': req.params.id},
// 			{
// 			  $pull: { engagements: {_id:req.params.id}}
// 			},{new:true},
// 			function(err, engagement) {
// 			  if(err){
// 					message = 'Echec de suppression de l\'element sélectionné : ' ;
// 					etat = 'KO';
// 					log('Delete', 'Echec de suppression de l\'engagement : ' + req.params.id,req.user.username);
// 				}else{
// 					message = 'Suppression avec succes de l\'element sélectionné';
// 					etat = 'OK';
// 					log('Delete', 'Suppression de l\'engagement : ' + req.params.id,req.user.username);
// 				}
// 				console.log('etat final : ', etat);
// 				res.json({ message: message, etat: etat });
// 			})
//
// });

router.post("/get", function(req, res, next){
	Exercices.findById(req.body.exerciceid, function(err, exercice){
		var engage = exercice.engagements.find(item => item._id == req.body.engageid)
		var op = exercice.engagements[exercice.engagements.indexOf(engage)].op.find(item => item._id == req.body.opid)
		res.json({result: "OK", op: op, ops: engage.op})
	})

});

router.get("/print/:id", function(req, res, next){
	console.log(req.body)
	var exerciceid = req.params.id.split('|')[0]
	var engageid = req.params.id.split('|')[1]
	var opid = req.params.id.split('|')[2]

	Exercices.findOne({ _id: exerciceid})
	.then((exercice) => {
		var engage = exercice.engagements.find(item => item._id == engageid)
		var op = engage.op.find(item => item._id == opid)

		var data = {
			op: op,
			exerciceid: exerciceid,
			engageid: engageid,
			beneficiaire: engage.beneficiaire,
			nummarche: engage.nummarche,
			numbc: engage.numbc,
			datebc: engage.datebc
		}
		// console.log(data)
		res.render("printop", {user: req.user, data: data, message: req.flash('message') })
		// res.json({result: "OK", data: data})
	})

});
router.get("/ov/:id", function(req, res, next){
	console.log(req.body)
	var exerciceid = req.params.id.split('|')[0]
	var engageid = req.params.id.split('|')[1]
	var opid = req.params.id.split('|')[2]

	Exercices.findOne({ _id: exerciceid})
	.then((exercice) => {
		var engage = exercice.engagements.find(item => item._id == engageid)
		var op = engage.op.find(item => item._id == opid)

		var data = {
			op: op,
			exerciceid: exerciceid,
			engageid: engageid,
			beneficiaire: engage.beneficiaire,
			nummarche: engage.nummarche,
			numbc: engage.numbc,
			datebc: engage.datebc
		}
		// console.log(data)
		res.render("ordresvirement", {user: req.user, data: data, message: req.flash('message') })
		// res.json({result: "OK", data: data})
	})

});


router.post('/validate', function(req, res, next){
	Exercices.findOne({ _id: req.body.exerciceid})
	.then((exercice) => {
		Counter.findOneAndUpdate({exerciceid: req.body.exerciceid}, {$inc: { seq_counterop: 1} }, function(error, counter){
		var engage = exercice.engagements.find(item => item._id == req.body.engageid)
		var d = new Date()
		var op = exercice.engagements[exercice.engagements.indexOf(engage)].op.find(item => item._id == req.body.opid)
		op.set({
			tmpflagop: false,
			numop: "OP".concat("00000".substring(0,"00000".length - counter.seq_counterop.toString().length), counter.seq_counterop).concat("/").concat(d.getFullYear())
		})
		exercice.save()
		.then(() => {
			res.json({result: "OK", messsage: "Opération réussie", op: op});
		})
		.catch((err) => {
			console.log("error :",err);
			res.json({result: "KO", message: err})
		})
		});
	})
});


router.post('/update', function(req, res, next){

	Exercices.findById(req.body.exerciceid, function(err, exercice){
		var engage = exercice.engagements.find(item => item._id == req.body.engageid)
		var op = exercice.engagements[exercice.engagements.indexOf(engage)].op.find(item => item._id == req.body.opid)
		// console.log(req.body.op.regflagop === true && req.body.op.visaflagop === false)
		// console.log(req.body.op.regflagop)
		// console.log(req.body.op.visaflagop)

		var moderegl = [
			{mode: '', counter: false, filter: {}},
			{mode: 'Chèque', counter: false, filter: {}},
			{mode: 'Virement', counter: true, filter: {exerciceid: req.body.exerciceid}, sufix: "OV"},
			{mode: 'MAD', counter: true, filter: {exerciceid: req.body.exerciceid}, sufix: "MAD"},
			{mode: 'ESP', counter: false, filter: {}}
		]

		var idregl = moderegl.find(item => item.mode === req.body.op.modereglement)
		if(idregl.counter === true){
			if (idregl.mode === 'Virement') {
				Counter.findOneAndUpdate(idregl.filter, {$inc: { seq_counterov: 1} }, function(error, counter){
					var optmp = req.body.op
					var d = new Date()
					optmp.numreglement = idregl.sufix.concat("00000".substring(0,"00000".length - counter.seq_counterov.toString().length), counter.seq_counterov).concat("/").concat(d.getFullYear())
					op.set(optmp)
					exercice.save()
					.then(function(err){
						res.json({result: "OK", message: "Opération réussie", op: op})
					})
					.catch(function(err){
						res.json({result: "KO", message: "Il y a eu un problème", err: err})
					})
				})
			} else if (idregl.mode === 'MAD') {
				Counter.findOneAndUpdate(idregl.filter, {$inc: { seq_countermad: 1} }, function(error, counter){
					var optmp = req.body.op
					var d = new Date()
					optmp.numreglement = idregl.sufix.concat("00000".substring(0,"00000".length - counter.seq_countermad.toString().length), counter.seq_countermad).concat("/").concat(d.getFullYear())
					op.set(optmp)
					exercice.save()
					.then(function(err){
						res.json({result: "OK", message: "Opération réussie", op: op})
					})
					.catch(function(err){
						res.json({result: "KO", message: "Il y a eu un problème", err: err})
					})
				})
			}


		}else{
			op.set(req.body.op)
			exercice.save()
			.then(function(err){
				res.json({result: "OK", message: "Opération réussie", op: op})
			})
			.catch(function(err){
				res.json({result: "KO", message: "Il y a eu un problème", err: err})
			})
		}
	})

})
router.post('/update_compta', function(req, res, next){

	Exercices.findById(req.body.exerciceid, function(err, exercice){
		var engage = exercice.engagements.find(item => item._id == req.body.engageid)
		var op = exercice.engagements[exercice.engagements.indexOf(engage)].op.find(item => item._id == req.body.opid)

			op.set(req.body.op)
			exercice.save()
			.then(function(err){
				res.json({result: "OK", message: "Opération réussie", op: op})
			})
			.catch(function(err){
				res.json({result: "KO", message: "Il y a eu un problème", err: err})
			})

	})

})


router.post('/delete', function(req, res, next){
	Exercices.findById(req.body.exerciceid, function(err, exercice){
		var engage = exercice.engagements.find(item => item._id == req.body.engageid)
		var op = exercice.engagements[exercice.engagements.indexOf(engage)].op.find(item => item._id == req.body.opid)
		// exercice.engagements[exercice.engagements.indexOf(engage)].op.splice(op,1)
		op.remove()
		exercice.save()
		.then(function(err){
			res.json({result: "OK", message: "Opération suppression de l'order de paiement réussie", op: engage.op, opun: op})
		})
		.catch(function(err){
			res.json({result: "KO", message: "Il y a eu un problème", err: err})
		})

	})

})


module.exports = router;


// {
// 	dateop: req.body.op.dateop,
// 	objetop: req.body.op.objetop,
// 	numfacture: req.body.op.numfacture,
// 	numbl: req.body.op.numbl,
// 	montantop: req.body.op.montantop,
// 	numbc: req.body.op.numbc,
// 	nummarche: req.body.op.nummarche,
// 	divers: req.body.op.divers,
// 	comptebancaire: req.body.op.comptebancaire,
// 	paiement: req.body.op.paiement,
// 	compta: req.body.op.compta,
// 	imputationbudget: req.body.op.imputationbudget
// }
