var express = require('express');
var router = express.Router();
var Projets = require('../models/projets.js');
var Rubriques = require('../models/rubriques.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).exer_projets == true){
		return next();
	}else{
		res.redirect('/index');
	}
}
/* GET users listing. */

router.get('/:id', isAuthenticated, havePermission, function(req, res, next) {

	Projets.findById(req.params.id,function(err, projetemploi){
		Rubriques.find({'active': true},function(err, rubriques){
		// console.log('koko',projetemploi.length);
		log('Show', 'Consultation de l\'emploi du projet : ' + req.params.id,req.user.username);
		res.render('projetemploi', {user: req.user, projetemplois: projetemploi, rubriques: rubriques, message: req.flash('message') });
	});
});
});

router.get('/api/get/:id',isAuthenticated, havePermission, function(req, res, next) {
	Projets.findById(req.params.id,function(err, projets){
			// res.json(exercice);
			res.json(projets);
	});

});


router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	Projets.find(function(err, projets){
		res.json(projets);
	});

});

router.post('/:id', isAuthenticated, havePermission, function(req, res, next){
		var total =0;
	  var programemploi = JSON.parse(req.body.programemploi);
		for (i = 0; i < programemploi.length; i++) {
		    total += Number(programemploi[i].creditouvert);
		}

		Projets.findById(req.params.id, function(err, projet){
			projet.update({
				emploi: programemploi,
				montant: total
			}, function(err, projetID){
				if(err){
					console.log(err)
				}else{
					res.redirect('/projets')
				}
			})
		})
	// })
})



module.exports = router;
