var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var Sourcesbudget = require('../models/sourcesbudget.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).exer_projets == true){
		return next();
	}else{
		res.redirect('/index');
	}
}
/* GET users listing. */

router.get('/:id', isAuthenticated, havePermission, function(req, res, next) {

	Exercices.findById(req.params.id,function(err, Exercices){
		Sourcesbudget.find(function(err, sourcesbudget){
		log('Show', 'Consultation des ressources de l\'exercice: ' + req.params.id,req.user.username);
		res.render('ressourcesexercice', {user: req.user, exeid: Exercices._id, ressourcesexercice: Exercices.ressbudget, sourcesbudget:sourcesbudget,codeexercice: Exercices.code,  message: req.flash('message') });
	});
});
});

router.get('/api/get/:id',isAuthenticated, havePermission, function(req, res, next) {
	Exercices.findById(req.params.id,function(err, exercices){
			// res.json(exercice);
			res.json(exercices.ressbudget);
	});

});


router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	Projets.find(function(err, projets){
		res.json(projets);
	});

});

router.post('/:id', isAuthenticated, havePermission, function(req, res, next){
var totalEquip_creditouvert =0;
var totalFonct_creditouvert =0;
var totalEquip_ress=0;
var totalFonct_ress =0;
var message ='';
var etat ='OK';

	  var ressourcesexercice = JSON.parse(req.body.ressourcesobj);

		Exercices.findById(req.params.id, function(err, exercice){
			// Controle depassement en cas de modofocation avec existance des credits ouverts
			for (var j=0; j < exercice.rubriques.length; j++){
							if(exercice.rubriques[j].typerubrique.toString() === 'EQUIPEMENT'){
								totalEquip_creditouvert += Number(exercice.rubriques[j].budget);
							}else {
								totalFonct_creditouvert += Number(exercice.rubriques[j].budget);
							}
			}

			for (var k=0; k < ressourcesexercice.length; k++){
							if(ressourcesexercice[k].typebudget.toString() === 'EQUIPEMENT'){
								totalEquip_ress += Number(ressourcesexercice[k].montantprevu);
							}else {
								totalFonct_ress += Number(ressourcesexercice[k].montantprevu);
							}
			}
			if((totalEquip_creditouvert <= totalEquip_ress) && (totalFonct_creditouvert <= totalFonct_ress)){
				exercice.update({
					ressbudget: ressourcesexercice
					// montant: total
				}, function(err, exerciceidID){
					if(err){
						console.log(err)
					}else{
						message = 'Modification du credit ouvert de la rubrique '
						//  + exercice.rubriques[j].code + ' de l exercice [' + exercice.code + ']';
						// res.redirect('/exercices')
						// res.json({ message: message , etat: etat});
					}
				})
			}
			// else {
			// 	message = 'Opération non effectuée. Le montant total par type ne doit pas dépasser le total des crédits ouverts.';
			// 	etat = 'KO';
			// 	res.json({ message: message , etat: etat});
			// }
		})
	// })
})

router.post('/api/check/:id', function(req, res, next){

			var totalEquip_creditouvert =0;
			var totalFonct_creditouvert =0;
			var totalEquip_ress=0;
			var totalFonct_ress =0;
			var message = '';
			var etat = 'OK';

      var ressourcesexercice = req.body

		Exercices.findById(req.params.id, function(err, exercice){
			// Controle depassement en cas de modofocation avec existance des credits ouverts
			for (var j=0; j < exercice.rubriques.length; j++){
							if(exercice.rubriques[j].typerubrique.toString() === 'EQUIPEMENT'){
								totalEquip_creditouvert += Number(exercice.rubriques[j].budget);
							}else {
								totalFonct_creditouvert += Number(exercice.rubriques[j].budget);
							}
			}

			for (var k=0; k < ressourcesexercice.length; k++){
							if(ressourcesexercice[k].typebudget.toString() === 'EQUIPEMENT'){
								totalEquip_ress += Number(ressourcesexercice[k].montantprevu);
							}else {
								totalFonct_ress += Number(ressourcesexercice[k].montantprevu);
							}
			}


			if((totalEquip_creditouvert <= totalEquip_ress) && (totalFonct_creditouvert <= totalFonct_ress)){
				message = 'Opération réussie';
				etat = 'OK';
				res.json({ message: message , etat: etat});
			}else {
				message = 'Opération non effectuée. Le montant total par type ne doit pas dépasser le total des crédits ouverts.';
				etat = 'KO';
				res.json({ message: message , etat: etat});
			}
		})



})



module.exports = router;
