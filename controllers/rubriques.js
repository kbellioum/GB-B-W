var express = require('express');
var router = express.Router();
var Rubriques = require('../models/rubriques.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).param_rubriques == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

/* GET users listing. */

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Rubriques.find(function(err, rubriques){
		log('Show', 'Consultation de la liste des rubriques.',req.user.username);
		res.render('rubriques', {user: req.user, rubriques: rubriques, message: req.flash('message') });
	});

});

router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	Rubriques.find(function(err, rubriques){
		res.json(rubriques);
	});

});

router.delete('/api/delete/:id',isAuthenticated, havePermission, function(req, res){
	var etat ='KO';
	var message = '';
	Rubriques.remove({
		_id: req.params.id
	}, function(err, rubrique) {
		if (err){
			message = 'Echec de suppression de l\'element sélectionné : ' ;
			etat = 'KO';
			log('Delete', 'Echec de suppression de la rubrique : ' + req.params.id,req.user.username);
			// res.send(err);

		}	else {
			log('Delete', 'Suppression de la rubrique : ' + req.params.id,req.user.username);
			message = 'Suppression avec succes de l\'element sélectionné  : ';
			etat = 'OK';
		}
		res.json({ message: message, etat: etat });
	});
});

router.post('/api/add',isAuthenticated, havePermission, function(req, res){
	var rubriques = new Rubriques();
	rubriques.code = req.body.code;
	rubriques.libelle = req.body.libelle;
	rubriques.typerubrique = req.body.typerubrique;
	rubriques.categorie = req.body.categorie;
	rubriques.active = req.body.active;
	var etat ='KO';
	var message = '';

	rubriques.save(function(err) {
			if (err){
					message =err;
					etat ='KO';
					log('Insert', 'Echec ajout de la rubrique: ' + req.body.libelle ,req.user.username);
					res.send(err);
				}
			else {
							message ='Opération terminée avec succès';
							etat ='OK';
							Rubriques.find(function(err, rubriques){
						  log('Insert', 'Ajout de la rubrique: ' + req.body.libelle ,req.user.username);
							res.json({ data: rubriques, message: message, etat: etat });
							});
					}
	});
});

router.post('/api/update/:id',isAuthenticated, havePermission, function(req, res){
		var code = req.body.code;
		var libelle = req.body.libelle;
		var typerubrique = req.body.typerubrique;
		var categorie = req.body.categorie;
		var active = req.body.active;
	  var etat ='KO';
	  var message ='';

		Rubriques.findById(req.params.id, function (err, rubriques) {
		rubriques.update({
		code: code,
		libelle: libelle,
		typerubrique: typerubrique,
		categorie: categorie,
		active: active
	},function (err, rubriquesID){
 		 if(err){
 			 message =err;
 			 etat ='KO';
 			 log('Update', 'Echec modification de la rubrique: ' + req.body.libelle ,req.user.username);
 			 console.log('GET Error: There was a problem retrieving: ' + err);
 		 }else{
 			 message ='Opération terminée avec succès';
 			 etat ='OK';
 			 log('Update', 'Modification de la rubrique : ' + req.body.libelle ,req.user.username);
 			 res.json({ message: message, etat: etat });
 		 }
 	 })

 	});
});





module.exports = router;
