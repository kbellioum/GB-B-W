var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).exer_raps == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

/* GET home page. */
router.get('/', isAuthenticated, havePermission, function(req, res, next) {
  res.render('rap', { title: 'RAPs', user: req.user, message: req.flash('message')});
});


router.get('/api/get', isAuthenticated, havePermission, function(req, res, next) {
  Exercices.find({rap: { $gt: 0 }, engagements: { $elemMatch: { exer_dest: "NONE" } }, clotured: true, disabled: false}, {}, {multi: true})
    .then((result) => {
      res.json({result: "OK", exernbr: result.length, exercices: result})
    })
});

router.get('/api/geteng/:id', isAuthenticated, havePermission, function(req, res, next) {
	Exercices.findById(req.params.id)
		.then((result) => {
			var engage = result.engagements.filter(item => item.rap > 0 && item.exer_dest == "NONE")
			res.json({result: "OK", engagement: engage})
		})
});


router.post('/api/getnatureeng', isAuthenticated, havePermission, function(req, res, next) {
	Exercices.findOne({ _id: req.body.exerciceid, clotured: false, disabled: false})
		.then((result) => {
			var tmp = result.engagements.filter(item => item.nature === "BC")
			var tab = []
			tmp.forEach((item) => {
				item.rubriques.forEach((item2) => {
					if(item2.code.trim() === req.body.code ){
						tab.push(item2)
					}
				})
			})
			var val = tab.reduce(function(a,b){
				return {montant: a.montant + b.montant}
			}).montant
			// res.json({result: "OK", rubriques: tab, montant: val})
			res.json({result: "OK", montant: val})
		})
		.catch((err) => {
			res.json({result: "KO", message: err})
		})
});

router.post('/api/cpeng/', isAuthenticated, havePermission, function(req, res, next) {
	Exercices.findOne({region: req.body.data.region, clotured: false, disabled: false})
		.then((result) => {

			var eng = req.body.data.engage
			if(eng.numeng.split("/").length == 2){
				eng.numeng = eng.numeng.concat("/RAP")
			}
			result.engagements.push(eng)
			Exercices.findOne({_id: req.body.data.exerciceid})
				.then((oldexercice) => {
					var oldeng = oldexercice.engagements.find(item => item._id == eng._id)
					oldeng.exer_dest = result._id
					Promise.all([result.save(), oldexercice.save()])
						.then(() => {
							res.json({result: "OK", engage: result, oldexercice: oldexercice, oldeng: oldeng})
						})
						.catch((err) => {
							res.json({result: "KO", message: err})
						})

				})


		})


});

module.exports = router;
