var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var core = require('../libs/core.js');
var Core = new core();
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).dashboard == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

// console.log(Core.get());
// console.log(Core.set());
// console.log("la version est : " + Core.getver());

// console.log(Core.getClients());
// console.log(Core.getClients());
// console.log(Core.getClients());



/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('dashboard', { title: 'Dashboard' });
// });
router.get('/', isAuthenticated, havePermission, function(req, res, next) {

		log('Show', 'Consultation de la liste des entites bénéficiaires.',req.user.username);
		res.render('dashboard', {user: req.user,  message: req.flash('message') });


});

module.exports = router;
