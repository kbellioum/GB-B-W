var express = require('express');
var router = express.Router();
var Comptescomptas = require('../models/comptescomptas.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");

var message ='';
var etat ='KO';

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).param_comptescompta == true){
		return next();
	}else{
		res.redirect('/index');
	}
}
/* GET users listing. = param_comptecomptabenef =*/

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Comptescomptas.find(function(err, comptescomptas){
		log('Show', 'Consultation de la liste des comptes comptables.',req.user.username);
		res.render('comptescomptas', {user: req.user, comptescomptas: comptescomptas, message: req.flash('message') });
	});
});

router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	Comptescomptas.find(function(err, comptescomptas){
		res.json(comptescomptas);
	});
});

router.delete('/api/delete/:id',isAuthenticated, havePermission, function(req, res){
	Comptescomptas.findByIdAndRemove(req.params.id, (err, comptecompta) => {
			  if (err){
					message = 'Echec de suppression de l\'element sélectionné : ' ;
					etat = 'KO';
					log('Delete', 'Echec de suppression du compte comptable : ' + req.params.id,req.user.username);
				}	else {
					log('Delete', 'Suppression du compte comptable : ' + req.params.id,req.user.username);
					message = 'Suppression avec succes de l\'element sélectionné  : ';
					etat = 'OK';
				}
				res.json({ message: message, etat: etat });
   });
});

router.post('/api/add',isAuthenticated, havePermission, function(req, res){

      var comptescomptas = new Comptescomptas();
      comptescomptas.code = req.body.code;
      comptescomptas.libelle = req.body.libelle;
			var etat ='KO';
			var message = '';

      comptescomptas.save(function(err) {
          if (err){
							message =err;
							etat ='KO';
						  log('Insert', 'Echec ajout du compte comptable: ' + req.body.libelle ,req.user.username);
							res.send(err);
						}
          else {
									message ='Opération terminée avec succès';
									etat ='OK';
								  Comptescomptas.find(function(err, comptescomptas){
									log('Insert', 'Ajout du compte comptable : ' + req.body.libelle ,req.user.username);
									// res.json({ data: comptescomptas, message: 'Entite successfully added!' });
									res.json({ data: comptescomptas, message: message, etat: etat });
									});
          		}
      });
});

router.post('/api/update/:id',isAuthenticated, havePermission, function(req, res){

		var code = req.body.code;
		var libelle = req.body.libelle;
		var etat ='KO';
		var message ='';

		Comptescomptas.findById(req.params.id, function (err, comptescomptas) {
		comptescomptas.update({
		code: code,
		libelle: libelle
	},function (err, comptescomptasID){
    		if(err){
					message =err;
					etat ='KO';
					log('Update', 'Echec modification du compte comptable : ' + req.body.libelle ,req.user.username);
    			console.log('GET Error: There was a problem retrieving: ' + err);
    		}else{
					message ='Opération terminée avec succès';
					etat ='OK';
					log('Update', 'Modification du compte comptable : ' + req.body.libelle ,req.user.username);
					res.json({ message: message, etat: etat });
				}
			})

		 });

	});





module.exports = router;
