var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).exer_crdouvert == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

/* GET users listing. = exer_crdouvert =*/

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	if(req.user.region.name === "All"){
		filter = {clotured: false}
	}else{
		filter = {region: req.user.region._id, clotured: false}
	}
	Exercices.find(filter, { _id: 1,libelle: 1 },function(err, exercices){
		log('Show', 'Consultation de la liste des credits ouverts.',req.user.username);
		res.render('creditsouverts', {user: req.user, exercices: exercices, message: req.flash('message') });
	}).sort( { createddate: -1 } );

});





router.post('/api/update/:id',isAuthenticated, havePermission, function(req, res){

	  var exeid = req.query.exeid;
	  var budget = req.body.budget;
		var message ='';
		var etat ='OK';
		var total_ress = 0;
		var total_creditouvert = 0;
		var typerubrique = req.body.typerubrique;
		var idrubrique = req.params.id;

		Exercices.findById(exeid,function(err, exercices){
			if (exercices.vcvalide === false){
				// controle depassement total ressources
				for (var k=0; k < exercices.ressbudget.length; k++){
								if(exercices.ressbudget[k].typebudget.toString() === typerubrique){
									total_ress += Number(exercices.ressbudget[k].montantprevu);
								}
				}
				for (var j=0; j < exercices.rubriques.length; j++){
								if((exercices.rubriques[j].typerubrique.toString() === typerubrique) && (exercices.rubriques[j]._id.toString() !== idrubrique)){
									total_creditouvert += Number(exercices.rubriques[j].budget);
								}
				}

				if(total_creditouvert + Number(budget) <= total_ress) {

						for (var i=0; i < exercices.rubriques.length; i++){
							if(exercices.rubriques[i]._id.toString() === idrubrique){
								if(exercices.rubriques[i].budget  === exercices.rubriques[i].montantdispo){
										var tt = exercices.rubriques[i];
										tt.budget = Number(budget);
										tt.montantdispo = Number(budget);
										exercices.rubriques[i] = tt;
										message = 'Modification du credit ouvert de la rubrique ' + exercices.rubriques[i].code + ' de l exercice [' + exercices.code + ']';
								}else{
										message = 'Opération non effectuée á cause de la présence des engagements.';
										etat = 'KO';
								}
								break;
							}

						}
				}else{
					message = 'Opération non effectuée á cause du dépassement du budget programmé des ressources .' + typerubrique;
					etat = 'KO';
				}

			} else {
					message = 'Opération non effectuée á cause de la présence d\'un virement de crédit.';
					etat = 'KO';
			}
					 if (etat === 'OK'){
										exercices.update({
											 rubriques: exercices.rubriques
										 },function (err, exercicesID){
											 if(err){
												 log('Update', message ,req.user.username);
												 console.log('GET Error: There was a problem retrieving: ' + err);
												 res.json({ message: message + err });
											 }else{
												 log('Update', message ,req.user.username);
												 res.json({ message: message, etat: etat });
											 }
										 })
								 } else {
									 log('Update', message ,req.user.username);
									 res.json({ message: message , etat: etat});
								 }
	 });

	});

module.exports = router;
