var express = require('express');
var router = express.Router();
var Counter = require('../models/counter.js');
var core = require('../libs/core.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).dep_engage == true){
		return next();
	}else{
		res.redirect('/index');
	}
}
// isAuthenticated, havePermission,

router.get('/api/get', function(req, res, next) {

  Counter.findOneAndUpdate({name: 'counterengage'}, {$inc: { seq: 1} }, function(error, counter)   {
    if(error){
      res.json(error);
    }else{
      res.json(counter);
    }
  });
});



module.exports = router;
