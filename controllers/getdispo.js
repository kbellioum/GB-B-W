var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var tool = require("../libs/mylib.js");

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).exer_gestexercice == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

/* GET users listing. */

// "59a8e0b2c440d130d7291db1"
// exercice : '59aacf3328163433a951d08d'
// rubrique : '5991ca47f782f73e31c23867'

router.get('/:id', function(req, res, next) {

  var id = { exerciceid: req.params.id.split('-')[0], rubriqueid: req.params.id.split('-')[1] }

  Exercices.findById(id.exerciceid, function (err, exercices){

    if (err)
        {
          res.send(err)
          console.log('err')
        }
    else {
      var obj = exercices.rubriques.find(function (item) {
        return item._id == id.rubriqueid;
      });
      res.json({
        budget: obj.budget,
        rubriqueid: obj._id,
        libelle: obj.libelle
      });
    }
  })

});

module.exports = router;
