var express = require('express');
var router = express.Router();
var ComptesBancaires = require('../models/comptesbancaires.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).param_cptbancaire == true){
		return next();
	}else{
		res.redirect('/index');
	}
}
/* GET users listing. = param_cptbancaire =*/

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	ComptesBancaires.find(function(err, comptesbancaires){
		log('Show', 'Consultation de la liste des comptes bancaires.',req.user.username);
		res.render('comptesbancaires', {user: req.user, comptesbancaires: comptesbancaires, message: req.flash('message') });
	});

});

router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	ComptesBancaires.find(function(err, comptesbancaires){
		res.json(comptesbancaires);
	});

});

router.delete('/api/delete/:id',isAuthenticated, havePermission, function(req, res){
	var etat ='KO';
	var message = '';
	ComptesBancaires.remove({
		_id: req.params.id
	}, function(err, comptebancaire) {
		if (err){
			message = 'Echec de suppression de l\'element sélectionné : ' ;
			etat = 'KO';
			log('Delete', 'Echec de suppression du compte bancaire : ' + req.params.id,req.user.username);
			// res.send(err);

		}	else {
			log('Delete', 'Suppression du compte bancaire: ' + req.params.id,req.user.username);
			message = 'Suppression avec succes de l\'element sélectionné  : ';
			etat = 'OK';
		}
		res.json({ message: message, etat: etat });
	});
});

router.post('/api/add',isAuthenticated, havePermission, function(req, res){
	var comptesbancaires = new ComptesBancaires();
	comptesbancaires.code = req.body.code;
	comptesbancaires.banque = req.body.banque;
	comptesbancaires.agence = req.body.agence;
	comptesbancaires.rib = req.body.rib;
	comptesbancaires.comptebancaire = req.body.comptebancaire;
	comptesbancaires.typebudget = req.body.typebudget;
	var etat ='KO';
	var message = '';

	comptesbancaires.save(function(err) {
			if (err){
					message =err;
					etat ='KO';
					log('Insert', 'Echec ajout du compte bancaire : ' + req.body.code ,req.user.username);
					res.send(err);
				}
			else {
							message ='Opération terminée avec succès';
							etat ='OK';
							ComptesBancaires.find(function(err, comptesbancaires){
							log('Insert', 'Ajout du compte bancaire : ' + req.body.code ,req.user.username);
							// res.json({ data: entites, message: 'Entite successfully added!' });
							res.json({ data: comptesbancaires, message: message, etat: etat });
							});

					}

	});
});

router.post('/api/update/:id',isAuthenticated, havePermission, function(req, res){
		var code = req.body.code;
		var banque = req.body.banque;
		var agence = req.body.agence;
		var rib = req.body.rib;
		var comptebancaire = req.body.comptebancaire;
		var typebudget = req.body.typebudget;
		var etat ='KO';
		var message ='';

	ComptesBancaires.findById(req.params.id, function (err, comptesbancaires) {
		comptesbancaires.update({
		code: code,
		banque: banque,
		agence: agence,
		rib: rib,
		comptebancaire: comptebancaire,
		typebudget: typebudget
	},function (err, rcomptesbancairesID){
			if(err){
				message =err;
				etat ='KO';
				log('Update', 'Echec modification du compte bancaire : ' + req.body.code ,req.user.username);
				console.log('GET Error: There was a problem retrieving: ' + err);
			}else{
				message ='Opération terminée avec succès';
				etat ='OK';
				log('Update', 'Modification du compte bancaire : ' + req.body.code ,req.user.username);
				res.json({ message: message, etat: etat });
			}
		})

	 });
	});





module.exports = router;
