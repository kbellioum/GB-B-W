var express = require('express');
var router = express.Router();
var Exercices = require('../models/exercices.js');
var Beneficiaires = require('../models/beneficiaires.js');
var Rubriques = require('../models/rubriques.js');
var Entites = require('../models/entites.js');
var Projets = require('../models/projets.js');
// var Counter = require('../models/counter.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).dep_engage == true){
		return next();
	}else{
		res.redirect('/index');
	}
}
router.get('/', function(req, res, next) {
	var filter = {}
	if(req.user.region.name === "All"){
		filter = {clotured: false}
	}else{
		filter = {region: req.user.region._id, clotured: false}
	}
	var exeid = req.params.id;

	Beneficiaires.find({ "type" : "Externe" }, { _id: 1,nom: 1 },function(err, beneficiairesext){
		Beneficiaires.find({ "type" : "Interne" }, { _id: 1,nom: 1 },function(err, beneficiairesint){
			Entites.find(function(err, entites){
					Rubriques.find(function(err, rubriques){
						Projets.find(function(err, projets){
							Exercices.find(filter, {_id: 1, code: 1, libelle: 1, clotured: 1, disabled: 1}, function(err, exercices){
								res.render('engagementsform', {user: req.user, exeid: exeid, beneficiairesext: beneficiairesext, exercices: exercices, beneficiairesint: beneficiairesint,entites: entites, rubriques: rubriques, projets: projets, message: req.flash('message') });
							});

						})
					});
 		   });
			});
   });
});

router.post('/api/add/:id', isAuthenticated, havePermission, function(req, res, next){
  var exerciceid = req.params.id;
	var userid = req.user._id;
	var beneficiairename = "";
	var objengage = {}
	var montanttotal = JSON.parse(req.body.detaileng).reduce(function(a, b) {
    return Number(a) + Number(b.montant);
  }, 0);

Beneficiaires.findById(req.body.beneficiaire,{ nom: 1},function(err, beneficiaire){
	beneficiairename = beneficiaire.nom;

		objengage = {
			numeng: "ENG_NO_VAL",
			dateeng: req.body.dateeng,
			beneficiaireid: req.body.beneficiaire,
			beneficiaire: beneficiairename,
			objet:req.body.objet,
			intitule: req.body.intitule,
			nature: req.body.nature,
			nummarche: req.body.nummarche,
			numbc: req.body.numbc,
			datebc: req.body.datebc,
			autreinfos: req.body.autreinfos,
			montanttotal: montanttotal,
			rubriques: JSON.parse(req.body.detaileng),
			userid: userid
		}

		Exercices.findById(req.params.id, function(err, exercice){
					exercice.engagements.push(objengage);
					exercice.save(function(err){
						if(err){
							res.send(err);
						}else{
							res.redirect('/engagements');
						}
					})
		});

});

});


	function js_yyyy_mm_dd_hh_mm_ss () {
		now = new Date();
		year = "" + now.getFullYear();
		month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
		day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
		hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
		minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
		second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
		return day + "/" + month + "/" + year + "  " + hour + ":" + minute + ":" + second;
	}

module.exports = router;
