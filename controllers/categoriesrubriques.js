var express = require('express');
var router = express.Router();
var Categoriesrubriques = require('../models/categoriesrubriques.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).param_categoriesrubriques == true){
		return next();
	}else{
		res.redirect('/index');
	}
}
/* GET users listing. = param_entitebenef =*/

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Categoriesrubriques.find(function(err, categoriesrubriques){
		log('Show', 'Consultation de la liste des catégories rubriques.',req.user.username);
		res.render('categoriesrubriques', {user: req.user, categoriesrubriques: categoriesrubriques, message: req.flash('message') });
	});

});

router.get('/api/get',isAuthenticated, havePermission, function(req, res, next) {
	Categoriesrubriques.find(function(err, categoriesrubriques){
		res.json(categoriesrubriques);
	});

});

router.delete('/api/delete/:id',isAuthenticated, havePermission, function(req, res){
	var etat ='KO';
	var message = '';
	Categoriesrubriques.remove({
		_id: req.params.id
	}, function(err, categorierubrique) {
		if (err){
			message = 'Echec de suppression de l\'element sélectionné : ' ;
			etat = 'KO';
			log('Delete', 'Echec de suppression de la catégorie rubrique : ' + req.params.id,req.user.username);
			// res.send(err);

		}	else {
			log('Delete', 'Suppression de la catégorie rubrique : ' + req.params.id,req.user.username);
			message = 'Suppression avec succes de l\'element sélectionné  : ';
			etat = 'OK';
			// res.json({ message: 'OK' });
		}
		res.json({ message: message, etat: etat });
	});
});

router.post('/api/add',isAuthenticated, havePermission, function(req, res){
	var categoriesrubriques = new Categoriesrubriques();
	categoriesrubriques.code = req.body.code;
	categoriesrubriques.libelle = req.body.libelle;
	var etat ='KO';
	var message = '';

	categoriesrubriques.save(function(err) {
			if (err){
					message =err;
					etat ='KO';
					log('Insert', 'Echec ajout de la catégorie rubrique : ' + req.body.libelle ,req.user.username);
					res.send(err);
				}
			else {
							message ='Opération terminée avec succès';
							etat ='OK';
							 Categoriesrubriques.find(function(err, categoriesrubriques){
							log('Insert', 'Ajout de la catégorie rubrique : ' + req.body.libelle ,req.user.username);
							// res.json({ data: entites, message: 'Entite successfully added!' });
							res.json({ data: categoriesrubriques, message: message, etat: etat });
							});
					}
	});
});

router.post('/api/update/:id',isAuthenticated, havePermission, function(req, res){
	var code = req.body.code;
	var libelle = req.body.libelle;
	var etat ='KO';
	var message ='';

	Categoriesrubriques.findById(req.params.id, function (err, categoriesrubriques) {
	categoriesrubriques.update({
	code: code,
	libelle: libelle
	},function (err, categoriesrubriquesID){
			if(err){
				message =err;
				etat ='KO';
				log('Update', 'Echec modification de la catégorie rubrique : ' + req.body.libelle ,req.user.username);
				console.log('GET Error: There was a problem retrieving: ' + err);
			}else{
				message ='Opération terminée avec succès';
				etat ='OK';
				log('Update', 'Modification de la catégorie rubrique : ' + req.body.libelle ,req.user.username);
				res.json({ message: message, etat: etat });
			}
		})
	 });

	});





module.exports = router;
