var express = require('express');
var router = express.Router();
var Recettes = require('../models/recettes.js');
var Comptesbancaires = require('../models/comptesbancaires.js');
var log = require('../libs/log.js');
var tool = require("../libs/mylib.js");


var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

var havePermission = function (req, res, next){
	var pp = new tool();
	if(pp.getValue(req.user.roleid).etat_recettes == true){
		return next();
	}else{
		res.redirect('/index');
	}
}

/* GET users listing.  = etat_recettes = */

router.get('/', isAuthenticated, havePermission, function(req, res, next) {
	Comptesbancaires.find(function(err, comptesbancaires){
		log('Show', 'Consultation de la liste des recettes.',req.user.username);
		res.render('recettes', {user: req.user, comptesbancaires: comptesbancaires, message: req.flash('message') });
	});

});

router.get('/api/get/:id',isAuthenticated, havePermission, function(req, res, next) {
  Comptesbancaires.findById(req.params.id,function(err, comptesbancaires){
		res.json(comptesbancaires.ecritures);
	});

});

router.delete('/api/delete/:id',isAuthenticated, havePermission, function(req, res){

        Recettes.remove({
          _id: req.params.id
        }, function(err, recette) {
          if (err){
						log('Delete', 'Echec de suppression de la recette : ' + req.params.id,req.user.username);
						res.send(err);
					}
					else {
						log('Delete', 'Suppression de la recette : ' + req.params.id,req.user.username);
						res.json({ message: 'Recette successfully deleted!' });
					}

        });

});

router.post('/api/cancel/:id',isAuthenticated, havePermission, function(req, res){

	var comptebancaireid = req.params.id;
  var ecritureid = req.query.ecritureid;
	var montant =  Number(req.body.montantmodal);
	var message ='';
	Comptesbancaires.findById(comptebancaireid,function(err, Comptesbancaires){

		for (var i=0; i < Comptesbancaires.ecritures.length; i++){

						if(Comptesbancaires.ecritures[i]._id.toString() === ecritureid){

									var tt = Comptesbancaires.ecritures[i];
									tt.annuler = true;
									tt.dateannul = js_yyyy_mm_dd_hh_mm_ss ();
									tt.motifannul = req.body.motifannulationmodal;
									Comptesbancaires.ecritures[i] = tt;
									message = 'Annulation de la recette du compte bancaire' + req.body.comptebancairemodal + ' D\'un montant de  ' + req.body.montantmodal;

							break;

						}
			}
		 Comptesbancaires.update({
				ecritures: Comptesbancaires.ecritures,
				solde: Comptesbancaires.solde - montant
			},function (err, comptebancaireID){
					 if(err){
						 log('Update', 'Echec annulation de la recette du compte bancaire : ' + req.body.comptebancairemodal + ' d\'un montant de ' +  req.body.montantmodal,req.user.username);
						 console.log('GET Error: There was a problem retrieving: ' + err);
					 }else{
						 log('Update', 'Annulation de la recette du compte bancaire : ' + req.body.comptebancairemodal + ' d\'un montant de ' +  req.body.montantmodal,req.user.username);
						res.redirect('/recettes');
					 }
				 });
		});
});


function js_yyyy_mm_dd_hh_mm_ss () {
	now = new Date();
	year = "" + now.getFullYear();
	month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
	day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
	hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
	minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
	second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
	return day + "/" + month + "/" + year + "  " + hour + ":" + minute + ":" + second;
}

module.exports = router;
