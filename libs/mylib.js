'use strict';
var Roles = require('../models/roles');
var Entites = require('../models/entites');

var tool = function () {
  this.permissions;
  this.entites;
  this.retValue = false;
  this.setValue = function(a){
  this.permissions = a;
  }
};



// tool.prototype.filterResult = function (selectedItems, table, section, champ) {
//   return table.filter(function (process) {
//     return process.section.some(function(item){
//       return selectedItems.indexOf(item.champ) > -1;
//     });
//   });
// }


// Roles & Permissions

tool.prototype.setPermission = function(data) {
    this.permissions = data;
};

tool.prototype.getPermission = function() {
   return this.permissions;
};

tool.prototype.getDataRoles = function(id) {

  var query = Roles.findById(id);
  return query;

};

var ql = {};

tool.prototype.getValue = function(roleid) {
  var query = this.getDataRoles(roleid);
  query.exec(function(err, d){
      if(err){
        console.log("Error . . . ");
      }else{
        ql = d;
      }
  });
  return ql;
};

//  Entites

tool.prototype.setEntites = function(data) {
    this.entites = data;
};

tool.prototype.getEntites = function() {
   return this.entites;
};


tool.prototype.getDataEntites = function(id) {

  var query = Entites.findById(id);
  return query;

};






module.exports = tool;
