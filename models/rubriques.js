var mongoose = require('mongoose');

module.exports = mongoose.model('Rubriques',{
  code: { type : String, required : true },
  libelle: String,
  typerubrique: String,
  categorie: String,
  budget: { type: Number, default: 0 },
  virementcredit: { type: Number, default: 0 },
  virementcredittmp: { type: Number, default: 0 },
  totalengagements: { type: Number, default: 0 },
  montantdispo: { type: Number, default: 0 },
  active: { type: Boolean, default: true }
});
