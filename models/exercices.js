var mongoose = require('mongoose');
var Counter = require('../models/counter.js');
var Projets = require('../models/projets.js');
var Region = require('../models/region.js');
var User = require('../models/user.js');
var Beneficiaires = require('../models/beneficiaires.js');


// var autoIncrement = require('mongoose-increment');
var Schema = mongoose.Schema;

var ExercicesSchema = new Schema({
	clotured: { type: Boolean, default: false },
	datecloture: Date,
	region: {
		type: Schema.Types.ObjectId,
		ref: 'Region'
	},
	rap: {
		type: Number,
		default: 0
	},
	regies: [{
		code: String,
	  libelle: String,
	  region: {
	    type: Schema.Types.ObjectId,
	    ref: 'Region'
	  },
	  regisseur: {
	    type: Schema.Types.ObjectId,
	    ref: 'Users'
	  },
	  emploi: [{code: String,
							libelle: String,
						  creditouvert: { type : Number , default : 0},
						  plafond: { type : Number , default : 0},
						  disponible: { type : Number , default : 0}
					  }],
		caisse: [{
	    date: String,
	    libelle: String,
			sens: String,
	    montant: Number,
	    user: {
	      type: Schema.Types.ObjectId,
	      ref: 'Users'
	    }
	  }],
	  journauxdecaisse: [
	    {
	      num: String,
				code: String,
	      datedebut: Date,
				datepostval: Date,
				dateval: Date,
	      detail: [{
					ressource: { type: String, default: 'BUDGET INRH'},
					ressourceid: { type: String, default: '59f4627be59c72694683091f'},
					code: String,
					libelle: String,
					entitebenef: String,
					description: String,
					montant: Number,
					dateop: { type: Date, default: Date.now}


	        // date: Date,
	        // rubrique: String,
	        // natop: String,
	        // recette: Number,
	        // depense: Number
	      }],
	      flagpostval:  { type: Boolean, default: false },
				datepostval: Date,
	      flagval:  { type: Boolean, default: false },
				dateval: Date


	    }
	  ]
	}],
	code: {
					type: String,
					default: '',
					trim: true,
					unique : true,
					required: 'Title cannot be blank'
				},
	libelle: {
						type: String,
						default: '',
						trim: true,
						required: 'Libelle cannot be blank'
					},
	disabled: {
    type: Boolean,
    default: false
  },
  user: String,
	date: String,
	createddate: { type: Date, default: Date.now },
	userlastupdate: String,
	datelastupdate: String,
	rubriques: [
		{
		code: { type : String },
		libelle: String,
		categorie: String,
		typerubrique: String,
		active: { type: Boolean, default: true },
		montantdispo: { type: Number, default: 0 },
		totalengagements: { type: Number, default: 0 },
		virementcredittmp: { type: Number, default: 0 },
		virementcredit: { type: Number, default: 0 },
		budget: { type: Number, default: 0 }

	}
],
	vcvalide: {
    type: Boolean,
    default: false
  },
	ressbudget: [{
			typebudget: String,
			ressb: String,
			montantprevu: Number
		}],
	engagements: [{
		rap: {
			type: Number,
			default: 0
		},
		exer_dest: {type: String, default: "NONE"},
		datecreate: {type: Date,default: new Date()},
		lastupdate: {type: Date,default: new Date()},
		numeng: String,
		tmpflag: { type: Boolean, default: true },
		dateeng: String,
		beneficiaireid: {
			type: Schema.Types.ObjectId,
			ref: 'Beneficiaires'
		},
		beneficiaire: String,
		objet: String,
		intitule: String,
		nature: String,
		nummarche: String,
		numbc: String,
		datebc: String,
		autreinfos: String,
		annuler: Boolean,
		dateannulation: String,
		montanttotal: Number,
		rubriques: [{
			ressource: String,
			ressourceid: String,
			// ressourceid: {
			// 	type: Schema.ObjectId,
			// 	ref: Projets,
			// 	default: "59f4627be59c72694683091f"
			// 	},
			code: String,
			libelle: String,
			entitebenef: String,
			montant: Number
		}],
		op: [{
			datecreate: String,
			lastupdate: String,
			ml: String,
			numop: String,
			dateop: String,
			objetop: String,
			beneficiaireopid: {
				type: Schema.Types.ObjectId,
				ref: 'Beneficiaires'
			},
			beneficiaireop: String,
			montantop: Number,
			justifs: Array,
			imputationbudget: Array,
			paiement: Array,
			comptebancaireid: String,
			comptebancaire: String,
			modereglement: String,
			numreglement: String,
			datereglement: String,
			datevisa: String,
			ovtyperegl: String,
			ovrib: String,
			ovdevise: Number,
			ovinterrib: String,
			ovbank: String,
			ovbankaccount: String,
			oviban: String,
			ovbic: String,
			annuler: { type: Boolean, default: false },
			dateannulation: Date,
			tmpflagop: { type: Boolean, default: true },
			regflagop: { type: Boolean, default: false },
			visaflagop: { type: Boolean, default: false },
			numjournal: String,
			numpiece: String,
			typecharge: String,
			comptaflagop: { type: Boolean, default: false },
			retenuflag: {type: Boolean, default: false}
		}],

	}]
});


module.exports = mongoose.model('Exercices', ExercicesSchema);
