var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var regiesparam = new Schema({
  code: String,
  libelle: String,
  region: {
    type: Schema.Types.ObjectId,
    ref: 'Region'
  },
  regisseur: {
    type: Schema.Types.ObjectId,
    ref: 'Users'
  },
  emploi: [{code: String,
						libelle: String,
					  creditouvert: { type : Number , default : 0},
					  plafond: { type : Number , default : 0},
					  disponible: { type : Number , default : 0}
				  }],
  caisse: [{
    date: Date,
    libelle: String,
    montant: Number,
    user: {
      type: Schema.Types.ObjectId,
      ref: 'Users'
    }
  }],
  journauxdecaisse: [
    {
      num: String,
      date: Date,
      detail: [{
        ressource: { type: String, default: 'BUDGET INRH'},
        ressourceid: { type: String, default: '59f4627be59c72694683091f'},
        code: String,
        libelle: String,
        entitebenef: String,
        montant: Number,
        dateop: { type: Date, default: Date.now},
        description: String
        // date: Date,
        // rubrique: String,
        // natop: String,
        // recette: Number,
        // depense: Number
      }],
      flagpostval: Boolean,
      datepostval: Date,
      flagval: Boolean,
      dateval: Date
    }
  ]
});

var RegiesParam = mongoose.model('RegiesParam', regiesparam);

module.exports = RegiesParam;
