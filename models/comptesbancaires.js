var mongoose = require('mongoose');

module.exports = mongoose.model('ComptesBancaires',{
	code: { type : String , unique : true, required : true },
	banque: { type : String , required : true },
	agence: String,
	rib: { type : String , unique : true, required : true },
	comptebancaire: { type : String , unique : true, required : true },
	typebudget: String,
	totaldebit: { type : Number , default : 0},
	totalcredit: { type : Number , default : 0},
	solde: { type : Number , default : 0},
	ecritures: [{
		numavis: Number,
		exercice: String,
		dateav: String,
		debiteur: String,
		objet: String,
		montant: Number,
		montantlettre: String,
		dateencaissement: String,
		sourcebudget: String,
		justif: String,
		// justif: [{
		// 	doc: String,
		// 	numero: String
		// }],
		comptagenerale: [{
			compte: String,
			libelle: String,
			debit: Number,
			credit: Number
		}],
		datesaisie: String,
		datevisa: String,
		annuler: { type : Boolean , default : false },
		dateannul: String,
		motifannul: String

	}]
});

// - Libelle
// - Date de début
// - Date de fin
