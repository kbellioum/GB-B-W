var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var RegionSchema = new Schema({
  name: String,
  ville: String
});


var Region = mongoose.model('Region', RegionSchema);


module.exports = Region;
