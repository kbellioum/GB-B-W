var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BeneficiaireSchema = new Schema({
	nom: String,
	type: String,
	tel: String,
	adresse: String,
	rib: String,
	cin: String,
	autreinfo: String,
	bankname: String,
	iban: String,
	bic: String
})


var Beneficiaires = mongoose.model('Beneficiaires', BeneficiaireSchema);
module.exports = Beneficiaires

// - Libelle
// - Date de début
// - Date de fin
