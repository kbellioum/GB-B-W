var mongoose = require('mongoose');

module.exports = mongoose.model('Recettes',{
	dateop: String,
	motif: String,
	user: String,
	montant: Number,
	sourcebudget: String,
	codecomptebancaire: String
});

// - Libelle
// - Date de début
// - Date de fin
