var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CounterSchema = new Schema({
  exerciceid: String,
  // name: String,
  // seq: {type: Number, default: 1}
  seq_counterengage: {type: Number, default: 1},
  seq_counterop: {type: Number, default: 1},
  seq_counterov: {type: Number, default: 1},
  seq_counterjc: {type: Number, default: 1},
  seq_countermad: {type: Number, default: 1}
});

module.exports = mongoose.model('Counter', CounterSchema);
