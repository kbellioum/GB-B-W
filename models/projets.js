var mongoose = require('mongoose');

module.exports = mongoose.model('Projets',{
	code: String,
	libelle: String,
	chef: String,
	autreinfo: String,
	createdate: String,
	usercreate: String,
	cloturer: { type: Boolean, default: false },
	emploi: [{code: String,
						libelle: String,
					  creditouvert: { type : Number , default : 0},
					  disponible: { type : Number , default : 0}
				  }],
	montant: {type: Number, default: 0},
	totalencaisser: {type: Number, default: 0},
	totalengager: {type: Number, default: 0},
	totalregler: {type: Number, default: 0},
	solde: {type: Number, default: 0}
});

// - Libelle
// - Date de début
// - Date de fin
