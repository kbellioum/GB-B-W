var mongoose = require('mongoose');
var Counter = require('../models/counter.js');
// var autoIncrement = require('mongoose-increment');
var Schema = mongoose.Schema;

var EngageTmpSchema = new Schema({
		numeng: String,
		dateeng: String,
		beneficiaire: String,
		objet: String,
		intitule: String,
		nature: String,
		nummarche: String,
		numbc: String,
		datebc: String,
		autreinfos: String,
		rubriques: [{
			ressource: String,
			code: String,
			libelle: String,
			entitebenef: String,
			montant: Number
		}],
		exercice: { _id: String, libelle: String },
		userid: String
});

// ExercicesSchema.plugin(autoIncrement, {
// 	type: String,
//   modelName: 'Exercices',
//   fieldName: 'engageid',
// 	 start: 300,
//   increment: 3
// });

module.exports = mongoose.model('EngageTmp', EngageTmpSchema);

function setallo(value){
	var counter = 0;
	Counter.findOneAndUpdate({name: 'counterengage'}, {$inc: { seq: 1} }, function(error, counter)   {
		if(error){

		}else{
			counter = counter.seq;
		}
	});
		console.log("counter : " + counter);
    return value.concat(counter);
}
