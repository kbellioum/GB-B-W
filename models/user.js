var bCrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');
var Region = require('../models/region.js');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
	// id: String,
	username: {type: String, unique: true, required: true},
	password: {type: String, set: createHash},
	email: String,
	firstName: String,
	lastName: String,
	roleid: String,
	regisseur: { type: Boolean, default: false },
	cin: String,
	region: {
			type: Schema.Types.ObjectId,
			ref: 'Region'
		}
});

module.exports = mongoose.model('User',UserSchema);

function createHash(password){
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}
