var mongoose = require('mongoose');

module.exports = mongoose.model('Roles',{
	nomrole: String,
	// menu administration
	dashboard: { type: Boolean, default: false },

	adm_user: { type: Boolean, default: false },
	adm_role: { type: Boolean, default: false },
	adm_trace: { type: Boolean, default: false },

	param_srcbudget: { type: Boolean, default: false },
	param_categoriesrubriques: { type: Boolean, default: false },
	param_rubriques: { type: Boolean, default: false },
	param_benef: { type: Boolean, default: false },
	param_cptbancaire: { type: Boolean, default: false },
	param_entitebenef: { type: Boolean, default: false },
	param_comptescompta: { type: Boolean, default: false },
	param_centres: { type: Boolean, default: false },
	param_regies: { type: Boolean, default: false },
	param_regions: { type: Boolean, default: false },

	exer_gestexercice: { type: Boolean, default: false },
	exer_crdouvert: { type: Boolean, default: false },
	exer_vircrd: { type: Boolean, default: false },
	exer_projets: { type: Boolean, default: false },
	exer_raps: { type: Boolean, default: false },
	exer_compta: { type: Boolean, default: false },

	regie_budget: { type: Boolean, default: false },
	regie_histocredit: { type: Boolean, default: false },
	regie_jc: { type: Boolean, default: false },
	regie_alimcaisse: { type: Boolean, default: false },

	rec_gestrecettes: { type: Boolean, default: false },

	dep_engage: { type: Boolean , default: false },
	dep_liquid: { type: Boolean , default: false },
	dep_paie: { type: Boolean , default: false },

	etat_crdouvert: { type: Boolean , default: false },
	etat_solde: { type: Boolean , default: false },
	etat_recettes: { type: Boolean , default: false },
	etat_engage: { type: Boolean , default: false },
	etat_annulengage: { type: Boolean , default: false },
	etat_annulop: { type: Boolean , default: false }

	});
