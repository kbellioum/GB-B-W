mongoimport -d GB -c comptesbancaires --type json --file comptesbancaires-bk.json
mongoimport -d GB -c entites --type json --file entites-bk.json
mongoimport -d GB -c exercices --type json --file exercices-bk.json
mongoimport -d GB -c roles --type json --file roles-bk.json
mongoimport -d GB -c rubriquess --type json --file rubriquess-bk.json
mongoimport -d GB -c sourcesbudgets --type json --file sourcesbudgets-bk.json
mongoimport -d GB -c users --type json --file users-bk.json
mongoimport -d GB -c beneficiaires --type json --file beneficiaires-bk.json
