const axios = require('axios')
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLSchema,
  GraphQLNonNull,
  GraphQLBoolean
} = require('graphql')

const Centres = require('../models/centres.js')
const Entites = require('../models/entites.js')

// Hardcoded Data

const customers = [
  {id: '1', name: 'Karim 1', email: 'karim1@gmail.com', age: 45},
  {id: '2', name: 'Karim 2', email: 'karim2@gmail.com', age: 40},
  {id: '3', name: 'Karim 3', email: 'karim3@gmail.com', age: 30},
  {id: '4', name: 'Karim 4', email: 'karim4@gmail.com', age: 20}
]

// Customer Type

const CustomerType = new GraphQLObjectType({
  name: 'Customer',
  fields: () => ({
    id: {type: GraphQLString},
    name: {type: GraphQLString},
    email: {type: GraphQLString},
    age: {type: GraphQLInt}
  })
})


const CentresType = new GraphQLObjectType({
  name: 'Centres',
  fields: () => ({
    _id: {type: GraphQLString},
    libelle: {type: GraphQLString},
    code: {type: GraphQLString}
  })
})

const EntitesType = new GraphQLObjectType({
  name: 'Entites',
  fields: () => ({
    _id: {type: GraphQLString},
    libelle: {type: GraphQLString},
    code: {type: GraphQLString}
  })
})



// Root Query
const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    customer: {
      type: CustomerType,
      args: {
        id: {type: GraphQLString}
      },
      resolve(parentValue, args) {
        return customers.find(item => item.id === args.id)
        // return axios.get('http://localhost:3000/customers/' + args.id)
        //           .then(res => res.data)
      }
    },
    centres: {
      type: GraphQLList(CentresType),
      resolve(parentValue, args) {

        return Centres.find()
          .then((result) => {
            return result
          })
        // return axios.get('http://localhost:3000/customers')
        //           .then(res => res.data)
      }
    },
    entites: {
      type: GraphQLList(EntitesType),
      resolve(parentValue, args) {

        return Entites.find()
          .then((result) => {
            return result
          })
        // return axios.get('http://localhost:3000/customers')
        //           .then(res => res.data)
      }
    },
    customers: {
      type: GraphQLList(CustomerType),
      resolve(parentValue, args) {
        return customers
        // return axios.get('http://localhost:3000/customers')
        //           .then(res => res.data)
      }
    }
  }
})

const mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addCustomer: {
      type: CustomerType,
      args: {
        name: {type: new GraphQLNonNull(GraphQLString)},
        email: {type: new GraphQLNonNull(GraphQLString)},
        age: {type: new GraphQLNonNull(GraphQLInt)}
      },
      resolve(parentValue, args) {
        return axios.post('http://localhost:3000/customers', {
          name: args.name,
          email: args.email,
          age: args.age
        })
        .then(res => res.data)
      }
    },
    deleteCustomer: {
      type: CustomerType,
      args: {
        id: {type: new GraphQLNonNull(GraphQLString)},
      },
      resolve(parentValue, args) {
        return axios.delete('http://localhost:3000/customers/' + args.id)
        .then(res => res.data)
      }
    },
    editCustomer: {
      type: CustomerType,
      args: {
        id: {type: new GraphQLNonNull(GraphQLString)},
        name: {type: GraphQLString},
        email: {type: GraphQLString},
        age: {type: GraphQLInt}
      },
      resolve(parentValue, args) {
        return axios.patch('http://localhost:3000/customers/' + args.id, args)
        .then(res => res.data)
      }
    }
  }
})

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation
})
